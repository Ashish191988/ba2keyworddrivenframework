package genericclasses;



import configuration.Resourse_path;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

import configuration.Resourse_path;
import driver.Driver;

	public class BenefitAsiaFunction {
		
		private static final String NewReCount = null;

		Hashtable<String, String> HRdata = new Hashtable<String, String>();
		Map<String, String> HRmap = new TreeMap<String, String>();
		
//		public static String empcsv = Resourse_path.csv_path+"/AutomationSG/Employee_1.csv";;
//		public static String depcsv = Resourse_path.csv_path+"/AutomationSG/Dependent_1.csv";
		
		public String new_empcsv = Resourse_path.csv_path+ GenericFunctions.ClientName + "/output_" + Driver.Empcsvfname;
		public String new_depcsv = Resourse_path.csv_path+GenericFunctions.ClientName + "/output_"+Driver.Depcsvfname;
		public String new_historycsv = Resourse_path.csv_path+GenericFunctions.ClientName + "/output_"+Driver.Histcsvfname;
		public String new_panelclaimscsv= Resourse_path.csv_path+GenericFunctions.ClientName + "/output_"+Driver.PClaimscsvfname;
		public String new_newhireEmpcsv= Resourse_path.csv_path+GenericFunctions.ClientName + "/output_"+Driver.newhireempcsvfname;
		public String new_newhireDepcsv= Resourse_path.csv_path+GenericFunctions.ClientName + "/output_"+Driver.newhiredepcsvfname;
		public String newHireflag = "";
		public ArrayList<String> LEcsvfname = new ArrayList<String>();
		public ArrayList<String> LEcsv_temp_fname = new ArrayList<String>();

		public static void main(String[] args) throws IOException, InterruptedException {
			// TODO Auto-generated method stub
//			UpdateEmpDepData cv = new UpdateEmpDepData();
//			cv.generateCode(empcsv,depcsv);
		}	
		
		public void generateCode(String empcsv, String depcsv, String historycsv, String claimscsv, String newEmpcsv,String newDepcsv,String LEDepcsv ) throws IOException{
			if (empcsv!=null)
			
			{
				updateEmpdata(empcsv);
			}
			if (depcsv!=null)
			
			{
				updateDep(depcsv);
			}
			if (historycsv!=null)
			{
					updateHistoryData(historycsv);
			}
			if(claimscsv!=null)
			{
				updateClaims(claimscsv);
			}
			
			if(newEmpcsv!=null)
			{
				newHireflag = "yes";
				updateEmpdata(newEmpcsv);
				newHireflag = "";
			}
			if(newDepcsv!=null)
			{
				newHireflag = "yes";
				updateDep(newDepcsv);
				newHireflag = "";
			}
			
			if(LEDepcsv!=null)
				
			{
				for (int i=1;i<=2;i++)
				{
					LEDepcsv = Resourse_path.csv_path + GenericFunctions.ClientName + "/" +"LE_Dep" + i+".csv";
					LEcsvfname.add(LEDepcsv);
					updateDep(LEDepcsv);
				}
				
			}
			
			if(LEDepcsv!=null)
			{
				deleteLEFiles(LEcsvfname);
				renameLEFiles(LEcsv_temp_fname, LEcsvfname);
			}
			
			
			
				
			deleteFile(empcsv,depcsv, historycsv, claimscsv, newEmpcsv, newDepcsv);
			renamfile(new_empcsv,empcsv,new_depcsv,depcsv, new_historycsv, historycsv, new_panelclaimscsv, claimscsv, new_newhireEmpcsv, newEmpcsv, new_newhireDepcsv, newDepcsv );
		}
		
		public void renameLEFiles(ArrayList<String> LEcsv_temp_fname, ArrayList<String> LEcsvfname)
		{
			
				for(int k=0; k<LEcsv_temp_fname.size();k++)
				{
					//get the new csv and old csv values				
					String newcsv = LEcsv_temp_fname.get(k);
					String oldcsv = LEcsvfname.get(k);
					File LEDep = new File(newcsv);
				//rename csv file
					if(LEDep.exists())
					{
					LEDep.renameTo(new File(oldcsv));
					}
			}
		}
		
		public void deleteLEFiles(ArrayList<String> LEcsvfname)
		{
			//String [] filename = {empcsv,depcsv,historycsv, claimscsv, newHEmpcsv,newHDepcsv };
			for(String x:LEcsvfname){
				if(x != null)
				{
					File file = new File(x);
					if(file.exists())
					{
					file.delete();
					}
				}
			}
		}
		
		public void renamfile( String new_empcsv, String empcsv,String new_depcsv, String depcsv, String new_historycsv, String historycsv, String new_panelclaimscsv, String claimscsv, String n_newHireEmpcsv, String newEmplcsv, String n_newHireDepcsv, String newDepecsv){
		//rename file
			if (empcsv!="")
			{
				File emp = new File(new_empcsv);
			//rename emp file
				if(emp.exists())
				{
				emp.renameTo(new File(empcsv));
				}
			}
			
			//rename dep file
			if (depcsv!="")
			{
				File Dep = new File(new_depcsv);
				if(Dep.exists())
				{
				Dep.renameTo(new File(depcsv));	
				}
			}
			if (historycsv!=null)
			{
				File historydata = new File(new_historycsv);
				//rename history file
				if(historydata.exists())
				{
					historydata.renameTo(new File(historycsv));
				}
			}
			if(claimscsv!=null)
			{
				File Claims = new File(new_panelclaimscsv);
				if(Claims.exists())
				{
					Claims.renameTo(new File(claimscsv));
				}
			}
			
			if(newEmplcsv!=null)
			{
				File newEmpcsv = new File(new_newhireEmpcsv);
				if(newEmpcsv.exists())
				{
					newEmpcsv.renameTo(new File(newEmplcsv));
				}
			}
			
			if(newDepecsv!=null)
			{
				File newDepcsv = new File(new_newhireDepcsv);
				if(newDepcsv.exists())
				{
					newDepcsv.renameTo(new File(newDepecsv));
				}
			}
			
		}
		
		public void deleteFile(String empcsv, String depcsv, String historycsv, String claimscsv, String newHEmpcsv, String newHDepcsv){
			String [] filename = {empcsv,depcsv,historycsv, claimscsv, newHEmpcsv,newHDepcsv };
			for(String x:filename){
				if(x != null)
				{
					File file = new File(x);
					if(file.exists())
					{
					file.delete();
					}
				}
			}
		}
		
		@SuppressWarnings("deprecation")
		public void updateEmpdata(String fName) throws IOException{
			String thisLine;
			int rowno = 0;
			FileInputStream fis = new FileInputStream(fName);
			DataInputStream myInput = new DataInputStream(fis);
			
			ArrayList<String> newdata = new ArrayList<String>();
			
			while ((thisLine = myInput.readLine()) != null) {
				ArrayList<String> olddata = new ArrayList<String>();
				
				String strar[] = thisLine.split(",");
				for(int m=0; m<strar.length; m++){
					olddata.add(strar[m]);
				}
				
				
				if (GenericFunctions.ClientName.equalsIgnoreCase("DBSG"))
				{
					if (rowno != 0) {
						String old_HRempID = olddata.get(0);
						String newHRID = getNewHRId(fName, old_HRempID);
						// System.out.println("old "+old_HRempID+" replaced with : "+newHRID);
						olddata.set(0, newHRID);
						olddata.set(1, newHRID);
						olddata.set(2, newHRID);
						olddata.set(7, newHRID);
						HRdata.put(old_HRempID, newHRID);
					}
				}
				if (GenericFunctions.ClientName.equalsIgnoreCase("DSOSG"))
				{
					if (rowno != 0)
					{
						String old_HRempID = olddata.get(0);
						String newHRID = getNewHRId(fName, old_HRempID);
						olddata.set(0, newHRID);
						olddata.set(1, newHRID);
						olddata.set(2, newHRID);
						olddata.set(3, newHRID);
						olddata.set(4, newHRID);
						olddata.set(5, newHRID);
						olddata.set(11, newHRID);
						
						HRdata.put(old_HRempID, newHRID);
				}
					
				}
				if (GenericFunctions.ClientName.equalsIgnoreCase("UBSHK"))
				{
					if (rowno != 0)
					{
						String old_HRempID = olddata.get(0);
						String newHRID = getNewHRId(fName, old_HRempID);
						olddata.set(0, newHRID);
						olddata.set(1, newHRID);
						olddata.set(2, newHRID);
						olddata.set(3, newHRID);
						olddata.set(4, newHRID);
						
						HRdata.put(old_HRempID, newHRID);
				}
					
				}
				if (GenericFunctions.ClientName.equalsIgnoreCase("BAMLSG"))
				{
					if (rowno != 0)
					{
						String old_HRempID = olddata.get(0);
						String newHRID = getNewHRId(fName, old_HRempID);
						olddata.set(0, newHRID);
						olddata.set(1, newHRID);
						olddata.set(2, newHRID);
						olddata.set(3, newHRID);
						
						HRdata.put(old_HRempID, newHRID);
					}
				}
				
				StringBuilder finaldata = new StringBuilder();
				
				for(int i=0; i<olddata.size(); i++){
					finaldata.append(olddata.get(i)+",");
				}
				
				String after_rem = finaldata.substring(0, finaldata.length()-1);
				newdata.add(after_rem);
				//assigning row
				rowno++;
			}	
			myInput.close();
			HRmap = new TreeMap<String, String>(HRdata); 
//			System.out.println("\n");
//			System.out.println("Employee details ");
//			System.out.println(" \n");
			for(int i=0; i<newdata.size(); i++){
//				System.out.println(newdata.get(i));
			}
			//write data to csv file
			FileWriter writer = null;
			if (newHireflag.equalsIgnoreCase("yes"))
			{
			writer = new FileWriter(new_newhireEmpcsv);
			}
			else
			{
				writer = new FileWriter(new_empcsv);
			}
			for (int i=0;  i < newdata.size(); i++) {
				writer.append(newdata.get(i));
				writer.append('\n');
			}
		    writer.flush();
		    writer.close();
			
		}

		public void updateDep(String fName) throws IOException{
			ArrayList<String> newdata = new ArrayList<String>();
			String new_LEDepcsv = null;
			int row = 0;		
			
			for(Map.Entry<String,String> map:HRmap.entrySet()){  
				   
				   String oldhr = map.getKey().toString();
				   String newhr = map.getValue().toString();

					String thisLine;
					FileInputStream fis = new FileInputStream(fName);
					DataInputStream myInput = new DataInputStream(fis);
			
					while ((thisLine = myInput.readLine()) != null) {
						
						ArrayList<String> olddata = new ArrayList<String>();
						String strar[] = thisLine.split(",");
						for(int m=0; m<strar.length; m++){
							olddata.add(strar[m]);
						}
						String HRID = olddata.get(0);
						if(row==0){
							StringBuilder finaldata = new StringBuilder();
							for(int i=0; i<olddata.size(); i++){
								finaldata.append(olddata.get(i)+",");
							}
							String after_rem = finaldata.substring(0, finaldata.length()-1);
							newdata.add(after_rem);
						}
						
					if (GenericFunctions.ClientName.equalsIgnoreCase("DBSG"))
						{

							if(oldhr.equals(HRID))
							{
								olddata.set(0, newhr);
								String FName = olddata.get(2);
								String Fname_Split[] = FName.split("_");
								String FnameNew = newhr + "_" + Fname_Split[3];
								String NIDTemp = olddata.get(6);
								String NID_Split[] = NIDTemp.split("_");
								String NIDNew = newhr+ "_" + NID_Split[3];
								String appendeddata = olddata.get(7);
								olddata.set(2, FnameNew);
								olddata.set(6, NIDNew+"_NID");

								StringBuilder finaldata = new StringBuilder();
								for(int i=0; i<olddata.size(); i++)
								{
									finaldata.append(olddata.get(i)+",");
								}
								String after_rem = finaldata.substring(0, finaldata.length()-1);
								newdata.add(after_rem);
							}
						}
						//if client is DSOSG
						if (GenericFunctions.ClientName.equalsIgnoreCase("DSOSG"))
						{
							if(oldhr.equals(HRID))
							{
								olddata.set(0, newhr);
								String FName = olddata.get(1);
								String Fname_Split[] = FName.split("_");
								String FnameNew = newhr + "_" + Fname_Split[3];
								String NIDTemp = olddata.get(4);
								String NID_Split[] = NIDTemp.split("_");
								String NIDNew = newhr+ "_" + NID_Split[3];
								String appendeddata = olddata.get(7);
								olddata.set(1, FnameNew);
								olddata.set(4, NIDNew+"_NID");

								StringBuilder finaldata = new StringBuilder();
								for(int i=0; i<olddata.size(); i++)
								{
									finaldata.append(olddata.get(i)+",");
								}
								String after_rem = finaldata.substring(0, finaldata.length()-1);
								newdata.add(after_rem);
							}
						}
						//if client is BAMLSG
						if (GenericFunctions.ClientName.equalsIgnoreCase("BAMLSG"))
						{
							if(oldhr.equals(HRID))
							{
								olddata.set(0, newhr);
								String FName = olddata.get(1);
								String Fname_Split[] = FName.split("_");
								String FnameNew = newhr + "_" + Fname_Split[3];
								String NIDTemp = olddata.get(4);
								String NID_Split[] = NIDTemp.split("_");
								String NIDNew = newhr+ "_" + NID_Split[3];
								String appendeddata = olddata.get(7);
								olddata.set(1, FnameNew);
								olddata.set(4, NIDNew+"_NID");

								StringBuilder finaldata = new StringBuilder();
								for(int i=0; i<olddata.size(); i++)
								{
									finaldata.append(olddata.get(i)+",");
								}
								String after_rem = finaldata.substring(0, finaldata.length()-1);
								newdata.add(after_rem);
							}
						}
						if (GenericFunctions.ClientName.equalsIgnoreCase("UBSHK"))
						{
							if(oldhr.equals(HRID))
							{
								olddata.set(0, newhr);
								String FName = olddata.get(2);
								String Fname_Split[] = FName.split("_");
								String FnameNew = newhr + "_" + Fname_Split[3];
								String NIDTemp = olddata.get(5);
								String NID_Split[] = NIDTemp.split("_");
								String NIDNew = newhr+ "_" + NID_Split[3];
								String appendeddata = olddata.get(7);
								olddata.set(2, FnameNew);
								olddata.set(5, NIDNew+"_NID");

								StringBuilder finaldata = new StringBuilder();
								for(int i=0; i<olddata.size(); i++)
								{
									finaldata.append(olddata.get(i)+",");
								}
								String after_rem = finaldata.substring(0, finaldata.length()-1);
								newdata.add(after_rem);
							}
						}
						
						//assigning row
						row++;
					}	
					myInput.close();
			 }

			
			for (int i=0;  i < newdata.size(); i++) {
//				System.out.println(newdata.get(i));
			}

			//write data to csv file
			FileWriter writer = null;
			if (newHireflag.equalsIgnoreCase("yes"))
			{
			writer = new FileWriter(new_newhireDepcsv);
			}
			else if (fName.contains("LE_Dep"))
			{
				String [] arr1 = fName.split("/");
				int length1 = arr1.length;
				String filename = arr1[length1-1];
				new_LEDepcsv= Resourse_path.csv_path+GenericFunctions.ClientName + "/output_"+filename;
				LEcsv_temp_fname.add(new_LEDepcsv);
				writer = new FileWriter(new_LEDepcsv);
			}
			else
			{
				writer = new FileWriter(new_depcsv);
			}
			
			for (int i=0;  i < newdata.size(); i++) {
				writer.append(newdata.get(i));
				writer.append('\n');
			}
		    writer.flush();
		    writer.close();
		}

		
		
		public void updateClaims(String fName) throws IOException{
			ArrayList<String> newdata = new ArrayList<String>();
			int row = 0;		
			
			for(Map.Entry<String,String> map:HRmap.entrySet())
			{  
				   
				Logger.info("beginning of search");   
				String oldhr = map.getKey().toString();
				   String newhr = map.getValue().toString();

					//String thisLine;
					//FileInputStream fis = new FileInputStream(fName);
					//DataInputStream myInput = new DataInputStream(fis);
					BufferedReader bufRdr;
					bufRdr = new BufferedReader(new FileReader(fName));
					String thisLine = null;
					while((thisLine = bufRdr.readLine()) != null)
					//while ((thisLine = myInput.readLine()) != null) 
					{					
						ArrayList<String> olddata = new ArrayList<String>();
						String strar[] = thisLine.split(",");
						for(int m=0; m<strar.length; m++)
						{
							olddata.add(strar[m]);
						}
						String HRID = olddata.get(3);
						if(row==0)
						{
							StringBuilder finaldata = new StringBuilder();
							for(int i=0; i<olddata.size(); i++)
							{
								finaldata.append(olddata.get(i)+",");
							}
							String after_rem = finaldata.substring(0, finaldata.length()-1);
							newdata.add(after_rem);
						}
						if(oldhr.equals(HRID))
						{
							olddata.set(3, newhr);
							String FName = olddata.get(8);
							String Fname_Split[] = FName.split("_");
							int Arrcount = Fname_Split.length;
							Logger.info("The size of the array is " + Arrcount);
							if (Arrcount > 3 )
							{
							String FnameNew = newhr + "_" + Fname_Split[3];
							olddata.set(8, FnameNew);
							}
							else
							{
								String FnameNew = newhr;
								olddata.set(8, FnameNew);
							}
							StringBuilder finaldata = new StringBuilder();
							for(int i=0; i<olddata.size(); i++)
							{
								finaldata.append(olddata.get(i)+",");
							}
							String after_rem = finaldata.substring(0, finaldata.length()-1);
							newdata.add(after_rem);
						}
						row++;
					}	
					//myInput.close();
					bufRdr.close();
			Logger.info("end of search");
		 }

			for (int i=0;  i < newdata.size(); i++) {
//				System.out.println(newdata.get(i));
			}

			//write data to csv file
			FileWriter writer = new FileWriter(new_panelclaimscsv);
			for (int i=0;  i < newdata.size(); i++) {
				writer.append(newdata.get(i));
				writer.append('\n');
			}
		    writer.flush();
		    writer.close();
		}

		
		
		public String getNewHRId(String file, String old_hrid) throws IOException{
			String hrid = "";
			String newString ="";
			
								
			String tempHead = old_hrid.toLowerCase();
        	      		
        	String temp = tempHead;
        		
        	String[] newArrTemp = temp.split("_");
			
        	newString = newArrTemp[0]+"_"+newArrTemp[1]+"_";
        	
			
			//String brk_oldhrid = old_hrid.substring(0, 9);
			String brk_oldhrid = newString;
			String brk_oldhridnum = newArrTemp[2];
			
			int num = Integer.parseInt(brk_oldhridnum);
			int totalrow = count(file);
			
			for(int i=0; i<totalrow; i++){
				String genID = brk_oldhrid+num;
//				System.out.println(" genID "+genID);
				hrid = genID;
				boolean st = returnRdStatus(file, genID);
				if(st==false){
					break;
				}
				num++;
			}
			return hrid;
		}
		
		public boolean returnRdStatus(String file, String old_hrid){
			boolean rt = false;
			String thisLine;
			try{
				ArrayList<String> storageID = new ArrayList<String>();
				storageID.add(old_hrid);
				
				if(storageID.size()>0){
					if(storageID.contains(old_hrid)){
						rt = true;
					}
				}
				
				FileInputStream fis = new FileInputStream(file);
				DataInputStream myInput = new DataInputStream(fis);
				int row = 0;
				while ((thisLine = myInput.readLine()) != null) {
					if(row!=0){
						String strar[] = thisLine.split(",");
						String orghrid = strar[0];
						if(old_hrid.equals(orghrid)){
							rt = true;
							break;
						}	
					}
					row++;
				}	
				myInput.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return rt;
		}
		
		public int count(String filename) throws IOException {
		    InputStream is = new BufferedInputStream(new FileInputStream(filename));
		    try {
		    byte[] c = new byte[1024];
		    int count = 0;
		    int readChars = 0;
		    boolean empty = true;
		    while ((readChars = is.read(c)) != -1) {
		        empty = false;
		        for (int i = 0; i < readChars; ++i) {
		            if (c[i] == '\n') {
		                ++count;
		            }
		        }
		    }
		    return (count == 0 && !empty) ? 1 : count;
		    } finally {
		    is.close();
		   }
		}

		/*
		
		public void updateHistoryData(String fName) throws IOException{
			ArrayList<String> newdata = new ArrayList<String>();
			int row = 0;		
			
			for(Map.Entry<String,String> map:HRmap.entrySet())
			{  
				   
				   String oldhr = map.getKey().toString();
				   String newhr = map.getValue().toString();

					String thisLine;
					FileInputStream fis = new FileInputStream(fName);
					DataInputStream myInput = new DataInputStream(fis);
			
					while ((thisLine = myInput.readLine()) != null) {
						
						ArrayList<String> olddata = new ArrayList<String>();
						String strar[] = thisLine.split(",");
						for(int m=0; m<strar.length; m++){
							olddata.add(strar[m]);
						}
						String HRID = olddata.get(0);
						if(row==0){
							StringBuilder finaldata = new StringBuilder();
							for(int i=0; i<olddata.size(); i++){
								finaldata.append(olddata.get(i)+",");
							}
							String after_rem = finaldata.substring(0, finaldata.length()-1);
							newdata.add(after_rem);
						}
						
//						System.out.println(oldhr+ " compared to "+HRID);
						if(oldhr.equals(HRID)){
							olddata.set(0, newhr);
							String appendeddata = olddata.get(7);
							olddata.set(2, newhr+"_"+appendeddata);
							olddata.set(6, newhr+"_"+appendeddata+"_NID");
							
							StringBuilder finaldata = new StringBuilder();
							for(int i=0; i<olddata.size(); i++){
								finaldata.append(olddata.get(i)+",");
							}
							String after_rem = finaldata.substring(0, finaldata.length()-1);
							newdata.add(after_rem);
						}
						
						//assigning row
						row++;
					}	
					myInput.close();
			 }
			
			for (int i=0;  i < newdata.size(); i++) {
			}

			//write data to csv file
			FileWriter writer = new FileWriter(new_historycsv);
			for (int i=0;  i < newdata.size(); i++) {
				writer.append(newdata.get(i));
				writer.append('\n');
			}
		    writer.flush();
		    writer.close();
		}
		
		
	}
*/

public void updateHistoryData(String fName) throws IOException{
			ArrayList<String> newdata = new ArrayList<String>();
			int row = 0;		
			
			for(Map.Entry<String,String> map:HRdata.entrySet()){  
				   
				   String oldhr = map.getKey().toString();
				   String newhr = map.getValue().toString();

					String thisLine;
					FileInputStream fis = new FileInputStream(fName);
					DataInputStream myInput = new DataInputStream(fis);
			
					while ((thisLine = myInput.readLine()) != null) {
						
						ArrayList<String> olddata = new ArrayList<String>();
						String strar[] = thisLine.split(",");
						for(int m=0; m<strar.length; m++){
							olddata.add(strar[m]);
						}
						String HRID = olddata.get(0);
						if(row==0){
							StringBuilder finaldata = new StringBuilder();
							for(int i=0; i<olddata.size(); i++){
								finaldata.append(olddata.get(i)+",");
							}
							String after_rem = finaldata.substring(0, finaldata.length()-1);
							newdata.add(after_rem);
						}
						
//						System.out.println(oldhr+ " compared to "+HRID);
						if(oldhr.equals(HRID)){
//							System.out.println("Operation performed!!");
							olddata.set(0, newhr);
							String FName = olddata.get(10);
							String Fname_Split[] = FName.split("_");
							String FnameNew = newhr + "_" + Fname_Split[3];
							olddata.set(10, FnameNew);
							
//							String appendeddata = olddata.get(10);
//							olddata.set(2, newhr+"_"+appendeddata);
							//olddata.set(10, newhr);
							
							StringBuilder finaldata = new StringBuilder();
							for(int i=0; i<olddata.size(); i++){
								finaldata.append(olddata.get(i)+",");
							}
							String after_rem = finaldata.substring(0, finaldata.length()-1);
							newdata.add(after_rem);
						}
						
						//assigning row
						row++;
					}	
					myInput.close();
			 }
			/*System.out.println("\n");
			System.out.println("history details ");
			System.out.println(" \n");*/
			
			for (int i=0;  i < newdata.size(); i++) {
				//System.out.println(newdata.get(i));
			}

			//write data to csv file
			FileWriter writer = new FileWriter(new_historycsv);
			for (int i=0;  i < newdata.size(); i++) {
				writer.append(newdata.get(i));
				writer.append('\n');
			}
		    writer.flush();
		    writer.close();
		}
	}
		


