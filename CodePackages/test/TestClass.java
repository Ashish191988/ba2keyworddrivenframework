package test;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.HashMap;

public class TestClass {
   
	public static void main(String []args){
		
		TestClass.executeRobotevent("");
		
		String fl  = "act_rb";
		String fn = fl.substring(0, 4);
		
		System.out.println(fn);
		
	}
	
    public static void executeRobotevent(String event_nm) {
        try {
               Robot robot = new Robot();
               HashMap<String, Integer> map = new HashMap<String, Integer>();

               map.put("rb_tab", 1);
               map.put("rb_enter", 2);
               map.put("rb_down", 3);
               map.put("rb_right", 4);
               
               int dataa = map.get(event_nm.toLowerCase());
               String msg = "operation perfomred!!";

               switch (dataa) {
               case 1:
                     robot.keyPress(KeyEvent.VK_TAB);
                     System.out.println(event_nm+" "+msg);
                     break;
               case 2:
                     robot.keyPress(KeyEvent.VK_ENTER);
                     System.out.println(event_nm+" "+msg);
                     break;
               case 3:
                     robot.keyPress(KeyEvent.VK_DOWN);
                     System.out.println(event_nm+" "+msg);
                     break;
               case 4:
                     robot.keyPress(KeyEvent.VK_RIGHT);
                     System.out.println(event_nm+" "+msg);    
                     break;
               default:
                     System.out.println("Event is not mapped for this operation");
                     break;
               }
        } catch (Exception e) {
               e.printStackTrace();
        }

 }

	
}
