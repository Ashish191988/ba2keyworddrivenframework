package genericclasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.poi.common.usermodel.Hyperlink;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;

import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Tlhelp32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.win32.W32APIOptions;

import driver.Driver;
import configuration.Resourse_path;

public class GenericFunctions {
	
	//Declared statics object and variables
	public static WebDriver driver;
	public static String Snapshot_flg;
	public static ArrayList<String> Snp_flag= new ArrayList<String>();
	public static String mainwindow;
	public static String resultmssg = null;
	public static int exe_rst_status = 0; // 0 for no report, 1 for pass and 2 for fail
	
	public static String returnresultmssg(){
		return resultmssg;
	}
	
	public static ArrayList<String> ReadDriverSuiteExcel() {
		ArrayList<String> Al = new ArrayList<String>();
		XSSFWorkbook Workbook_obj = null;
		try {
			String Driversheetpath = Resourse_path.Driver_Sheetpath;
			FileInputStream FIS = new FileInputStream(Driversheetpath);
			Workbook_obj = new XSSFWorkbook(FIS);
			XSSFSheet sheet_obj = Workbook_obj.getSheet("Driver");
			int Row_count = sheet_obj.getLastRowNum();
			// System.out.print(Row_count+"\n");
			for (int i = 1; i <= Row_count; i++) {
				XSSFRow row_obj = sheet_obj.getRow(i);
				XSSFCell cell_obj = row_obj.getCell(3);
				String Exec_indicator = cell_obj.getStringCellValue();
				// System.out.print(Exec_indicator+"\n");
				String Exec_ind = Exec_indicator.trim();
				if (Exec_ind.equalsIgnoreCase("Y")) {
					XSSFCell cellobj1 = row_obj.getCell(1);
					String Sheetname = cellobj1.getStringCellValue();
					Al.add(Sheetname);
					XSSFCell cellobj4 = row_obj.getCell(4);
					Snapshot_flg = cellobj4.getStringCellValue();
					Snp_flag.add(Snapshot_flg);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}finally{
			try {
				Workbook_obj.close();
			} catch (IOException e) {
				e.printStackTrace();
				Logger.error(e);
			}
		}
		return Al;
	}
	
	
	public static ArrayList<String> FindTestData() {
		String TestData_Sheetpath = Resourse_path.TestData_Sheetpath+ Driver.DriverSheetname + ".xlsx";
//		String TestData_Sheetpath = Resourse_path.TestData_Sheetpath+"BOFAMLHK" + ".xlsx";
		ArrayList<String> Dl = new ArrayList<String>();
		XSSFWorkbook Wbook_obj = null;
		String stp = Driver.StepNumber;
		String script_Tcid = Driver.Testcasename;
		String keyword = Driver.KeywordName;
		String TestData_SheetName = Driver.SuiteName;
		
		try {
			FileInputStream FIS = new FileInputStream(TestData_Sheetpath);
			Wbook_obj = new XSSFWorkbook(FIS);
			XSSFSheet Wsheet_obj = Wbook_obj.getSheet(TestData_SheetName);
			int Rowcount = Wsheet_obj.getLastRowNum();
			int RequiredRow = 0;

			for (int i = 1; i <= Rowcount; i++) {
				//creating row object
				XSSFRow rowobj = Wsheet_obj.getRow(i);
				//Storing values to string variable
				XSSFCell Cell_obj = rowobj.getCell(0);
				String Excel_stepnum = Cell_obj.getStringCellValue();
				XSSFCell Cell_obj1 = rowobj.getCell(1);
				String Excel_Tcid = Cell_obj1.getStringCellValue();
				XSSFCell Cell_obj2 = rowobj.getCell(4);
				String Excel_Keyword = Cell_obj2.toString();
				
				if (stp.equalsIgnoreCase(Excel_stepnum) && Excel_Keyword.equalsIgnoreCase(keyword) && 
						Excel_Tcid.equalsIgnoreCase(script_Tcid)) {
					RequiredRow = i;
					break;
				}
				
			}
            //column value is added in the list
			XSSFRow RRow_obj = Wsheet_obj.getRow(RequiredRow);
			int Cell_count = RRow_obj.getLastCellNum();
			for (int j = 5; j <= Cell_count - 1; j++) {
				XSSFCell Rcell_obj = RRow_obj.getCell(j);
				if(Rcell_obj!=null){
					String cell_val = Rcell_obj.getStringCellValue();
					Dl.add(cell_val);
				}
			}
			
		} catch (Exception e) {
            e.printStackTrace();
            Logger.error(e);
		}finally {
			try {
				Wbook_obj.close();
			  } catch (IOException e) {
				e.printStackTrace();
				Logger.error(e);
			}	
		}
		return Dl;
	}
	
	public static void CreateHighLevelResult(String Result_SheetName) {
		try {
			String Result_Sheetpath = Driver.HighLevel_Result_Sheetpath;
			String[] Result_Sheet_header = Resourse_path.HighLevel_Result_header;
            //Excel operation
			XSSFWorkbook Wbook_obj = new XSSFWorkbook();
			XSSFSheet Wsheet_obj = Wbook_obj.createSheet(Result_SheetName);
			XSSFRow Row_obj = Wsheet_obj.createRow(0);
			int Col_loop = Result_Sheet_header.length;
			for (int i = 0; i < Col_loop; i++) {
				XSSFCell cell_obj = Row_obj.createCell(i);
				cell_obj.setCellValue(Result_Sheet_header[i]);
				CellStyle cellStyleobj = SetCellstyl(Wbook_obj);
				cell_obj.setCellStyle(cellStyleobj);
				Wsheet_obj.autoSizeColumn(i);
			}
			FileOutputStream FOS = new FileOutputStream(Result_Sheetpath);
			Wbook_obj.write(FOS);
			FOS.close();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	
	public static void Format_HL_Result(String arr[], String Result_SheetName) {
		try {
			String Result_Sheetpath = Driver.HighLevel_Result_Sheetpath;

			FileInputStream FIS = new FileInputStream(Result_Sheetpath);
			Workbook Wbook_obj1 = WorkbookFactory.create(FIS);
			
			if (Wbook_obj1.getSheet(Result_SheetName) == null) {
				String[] Result_Sheet_header = Resourse_path.HighLevel_Result_header;
				FileInputStream N_FIS = new FileInputStream(Result_Sheetpath);
				Workbook Wbook_obj = WorkbookFactory.create(N_FIS);
				Sheet Wsheet = Wbook_obj.createSheet(Result_SheetName);
				
				Row Row_obj = Wsheet.createRow(0);
				int Col_loop = Result_Sheet_header.length;
				
				for (int i = 0; i < Col_loop; i++) {
					Cell cell_obj = Row_obj.createCell(i);
					cell_obj.setCellValue(Result_Sheet_header[i]);
					CellStyle cellStyleobj = SetCellstyl(Wbook_obj);
					cell_obj.setCellStyle(cellStyleobj);
					Wsheet.autoSizeColumn(i);
				}
				FileOutputStream FOS = new FileOutputStream(Result_Sheetpath);
				Wbook_obj.write(FOS);
				FOS.close();

			}
			FIS.close();
			
			//Second operation
			FileInputStream FIS_1 = new FileInputStream(Result_Sheetpath);
			Workbook Wbook_obj = WorkbookFactory.create(FIS_1);
			Sheet Wsheet_obj = Wbook_obj.getSheet(Result_SheetName);
			
			int Rowcount = Wsheet_obj.getLastRowNum();
			int ReqRow_Num = Rowcount + 1;
			Row NSobj = Wsheet_obj.createRow(ReqRow_Num);
			
			for (int i = 0; i < arr.length; i++) {
				// String Value = null;
				String Value = arr[i];
				
				Cell CellObj = NSobj.createCell(i);

				if (i != 4) {
					CellObj.setCellValue(Value);
					CellStyle Borderstyle_obj = GenericFunctions.SetCellBorderstyl(Wbook_obj);
					CellObj.setCellStyle(Borderstyle_obj);
				}
				else if (i == 4) {
					if (Value == "Passed") {
						CellStyle CellStyleObj = Wbook_obj.createCellStyle();
						Font Fontobj = Wbook_obj.createFont();
						Fontobj.setFontHeightInPoints((short) 11);
						Fontobj.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
						Fontobj.setColor(IndexedColors.GREEN.getIndex());
						CellStyleObj.setFont(Fontobj);
						CellStyleObj.setBorderLeft(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setBorderRight(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setBorderTop(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setBorderBottom(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setLeftBorderColor(IndexedColors.BLACK.getIndex());
						CellStyleObj.setRightBorderColor(IndexedColors.BLACK.getIndex());
						CellStyleObj.setTopBorderColor(IndexedColors.BLACK	.getIndex());
						CellStyleObj.setBottomBorderColor(IndexedColors.BLACK.getIndex());
						CellObj.setCellStyle(CellStyleObj);
						CellObj.setCellValue(Value);
					} 
					else if (Value == "Failed") {
						CellStyle CellStyleObj = Wbook_obj.createCellStyle();
						Font Fontobj = Wbook_obj.createFont();
						Fontobj.setFontHeightInPoints((short) 11);
						Fontobj.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
						Fontobj.setColor(IndexedColors.RED.getIndex());
						CellStyleObj.setFont(Fontobj);
						CellStyleObj.setBorderLeft(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setBorderRight(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setBorderTop(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setBorderBottom(XSSFCellStyle.BORDER_THIN);
						CellStyleObj.setLeftBorderColor(IndexedColors.BLACK.getIndex());
						CellStyleObj.setRightBorderColor(IndexedColors.BLACK.getIndex());
						CellStyleObj.setTopBorderColor(IndexedColors.BLACK.getIndex());
						CellStyleObj.setBottomBorderColor(IndexedColors.BLACK.getIndex());
						CellObj.setCellStyle(CellStyleObj);
						CellObj.setCellValue(Value);
					}
				}
			}
			
			FileOutputStream FOS = new FileOutputStream(Result_Sheetpath);
			Wbook_obj.write(FOS);
			FOS.close();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
	}

	public static void Write_HL_ResultToExcel(String Res_Arr [], String Sheetname) {
		GenericFunctions.Format_HL_Result(Res_Arr,Sheetname+"_High_Level");
	}
	
	public static void CreateResultExcel(String Result_SheetName){
		try{
			String Result_Sheetpath = Driver.LowLeveL_Result_Sheetpath;
			// String Result_SheetName=Resourse_path.Result_SheetName;
			String[] Result_Sheet_header = Resourse_path.Result_Sheet_header;

			XSSFWorkbook Wbook_obj = new XSSFWorkbook();
			XSSFSheet Wsheet_obj = Wbook_obj.createSheet(Result_SheetName);
			XSSFRow Row_obj = Wsheet_obj.createRow(0);
			int Col_loop = Result_Sheet_header.length;
			for (int i = 0; i < Col_loop; i++) {
				XSSFCell cell_obj = Row_obj.createCell(i);
				cell_obj.setCellValue(Result_Sheet_header[i]);
				CellStyle cellStyleobj = SetCellstyl(Wbook_obj);
				cell_obj.setCellStyle(cellStyleobj);
				Wsheet_obj.autoSizeColumn(i);
			}
			File files = new File(Driver.LowLevel_Result_Folder);
			if (!files.exists()) {
				files.mkdirs();
			}
			FileOutputStream FOS = new FileOutputStream(Result_Sheetpath);
			Wbook_obj.write(FOS);
			FOS.close();
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	
 //************************** Code for font ***********************
	public static Font Fontstyle(Workbook wbookobj, short fontheight) {
		Font Fontobj = wbookobj.createFont();
		Fontobj.setFontHeightInPoints(fontheight);
		Fontobj.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		Fontobj.setColor(IndexedColors.WHITE.getIndex());
		return Fontobj;
	}
	
	//********************* Code for Cell style ***************
	public static CellStyle SetCellBorderstyl(Workbook wbookobj) {
		CellStyle cellBorderStyleobj = wbookobj.createCellStyle();
		cellBorderStyleobj.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		cellBorderStyleobj.setBorderRight(XSSFCellStyle.BORDER_THIN);
		cellBorderStyleobj.setBorderTop(XSSFCellStyle.BORDER_THIN);
		cellBorderStyleobj.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		cellBorderStyleobj.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellBorderStyleobj.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellBorderStyleobj.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cellBorderStyleobj.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		return cellBorderStyleobj;
	}

	public static CellStyle SetCellstyl(Workbook wbookobj) {
		CellStyle cellStyleobj = wbookobj.createCellStyle();
		short colouInd = IndexedColors.BLACK.getIndex();
		short fillpatern = CellStyle.SOLID_FOREGROUND;
		short fontheight = 11;
		cellStyleobj.setFillForegroundColor(colouInd);
		cellStyleobj.setFillPattern(fillpatern);
		Font Fontobj = Fontstyle(wbookobj, fontheight);
		cellStyleobj.setFont(Fontobj);
		return cellStyleobj;
	}

	public static CellStyle SetCellstyl_LLResult(Workbook wbookobj) {
		CellStyle cellStyleobj = wbookobj.createCellStyle();
		short colouInd = IndexedColors.YELLOW.getIndex();
		short fillpatern = CellStyle.SOLID_FOREGROUND;
		short fontheight = 11;
		cellStyleobj.setFillForegroundColor(colouInd);
		cellStyleobj.setFillPattern(fillpatern);
		Font Fontobj1 = Fontstyle_LLResult(wbookobj, fontheight);
		cellStyleobj.setFont(Fontobj1);
		return cellStyleobj;
	}

	public static Font Fontstyle_LLResult(Workbook wbookobj, short fontheight) {
		Font Fontobj1 = wbookobj.createFont();
		Fontobj1.setFontHeightInPoints(fontheight);
		Fontobj1.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		Fontobj1.setColor(IndexedColors.BLACK.getIndex());
		return Fontobj1;
	}
	
	public static void AddRowToResult(String Result_SheetName) {
		try {
			String Result_Sheetpath = Driver.LowLeveL_Result_Sheetpath;
			Logger.info("Result_Sheetpath" + Result_Sheetpath);
			FileInputStream FIS = new FileInputStream(Result_Sheetpath);
			Workbook Wbook_obj = WorkbookFactory.create(FIS);
			Sheet Wsheet_obj = Wbook_obj.getSheet(Result_SheetName+ "_Detailed_Results");
			int LastRownum = Wsheet_obj.getLastRowNum();
			int requiredrow = LastRownum + 1;
			Row Row_obj = Wsheet_obj.createRow(requiredrow);
			for (int i = 0; i < 9; i++) {
				Cell cell_obj = Row_obj.createCell(i);
				CellStyle cellStyleobj = SetCellstyl_LLResult(Wbook_obj);
				cell_obj.setCellStyle(cellStyleobj);
				Wsheet_obj.autoSizeColumn(i);
			}
			// Cell_obj.setCellValue("END");
			FileOutputStream FOS = new FileOutputStream(Result_Sheetpath);
			Wbook_obj.write(FOS);
			FOS.close();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	
	public static void FormatResultExcel(String arr [],String Result_SheetName) throws IOException, InvalidFormatException{
		
		String Result_Sheetpath = Driver.LowLeveL_Result_Sheetpath;
		FileInputStream FIS_1 = new FileInputStream(Result_Sheetpath);
		Workbook Wbook_obj = WorkbookFactory.create(FIS_1);
		Sheet Wsheet_obj = Wbook_obj.getSheet(Result_SheetName);
		
		int Rowcount = Wsheet_obj.getLastRowNum();
		int ReqRow_Num = Rowcount + 1;
		Row NSobj = Wsheet_obj.createRow(ReqRow_Num);
		int lencmp = arr.length;
//		Logger.info("lencmp "+lencmp);
		String exmsg = null;
		if(lencmp==10){
			exmsg = arr[9];
		}
		for (int i = 0; i < arr.length; i++) {
			String Value = arr[i];
			Cell CellObj = NSobj.createCell(i);
			if (i != 7 && i != 8 && i!=9) {
				CellObj.setCellValue(Value);
				CellStyle Borderstyle_obj = GenericFunctions.SetCellBorderstyl(Wbook_obj);
				CellObj.setCellStyle(Borderstyle_obj);
			} else if (i == 8) {
				// Logger.info("i==8");
				CellStyle hlink_style = Wbook_obj.createCellStyle();
				Font hlink_font = Wbook_obj.createFont();
				hlink_font.setUnderline(Font.U_SINGLE);
				hlink_font.setColor(IndexedColors.BLUE.getIndex());
				hlink_style.setFont(hlink_font);
				CellObj.setCellValue("Link");
				CreationHelper createHelper = Wbook_obj.getCreationHelper();

				if(Value!=null){
					Hyperlink link = createHelper.createHyperlink(Hyperlink.LINK_FILE);
					Value = Value.replace("\\", "/");
					link.setAddress(Value);
					CellObj.setHyperlink((org.apache.poi.ss.usermodel.Hyperlink) link);
					CellObj.setCellStyle(hlink_style);
				}
				
			} else if (i == 7) {
				if (Value == "Passed") {
					CellStyle CellStyleObj = Wbook_obj.createCellStyle();
					Font Fontobj = Wbook_obj.createFont();
					Fontobj.setFontHeightInPoints((short) 11);
					Fontobj.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
					Fontobj.setColor(IndexedColors.GREEN.getIndex());
					CellStyleObj.setFont(Fontobj);
					CellStyleObj.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setBorderRight(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setBorderTop(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setLeftBorderColor(IndexedColors.BLACK
							.getIndex());
					CellStyleObj.setRightBorderColor(IndexedColors.BLACK
							.getIndex());
					CellStyleObj.setTopBorderColor(IndexedColors.BLACK
							.getIndex());
					CellStyleObj.setBottomBorderColor(IndexedColors.BLACK
							.getIndex());

					CellObj.setCellStyle(CellStyleObj);
					CellObj.setCellValue(Value);
				} else if (Value == "Failed") {

					CellStyle CellStyleObj = Wbook_obj.createCellStyle();
					Font Fontobj = Wbook_obj.createFont();
					Fontobj.setFontHeightInPoints((short) 11);
					Fontobj.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
					Fontobj.setColor(IndexedColors.RED.getIndex());
					CellStyleObj.setFont(Fontobj);
					CellStyleObj.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setBorderRight(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setBorderTop(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					CellStyleObj.setLeftBorderColor(IndexedColors.BLACK
							.getIndex());
					CellStyleObj.setRightBorderColor(IndexedColors.BLACK
							.getIndex());
					CellStyleObj.setTopBorderColor(IndexedColors.BLACK
							.getIndex());
					CellStyleObj.setBottomBorderColor(IndexedColors.BLACK
							.getIndex());
					CellObj.setCellStyle(CellStyleObj);
					CellObj.setCellValue(Value);
					
					if(lencmp==10){
						if(exmsg!=null){
							fillcommentinexcel(Wbook_obj,Wsheet_obj,NSobj,CellObj,exmsg);
						}else{
							Logger.info("exmsg msg "+exmsg);
						}
					}
					
				}
			}
			Wsheet_obj.autoSizeColumn(i);
		}
		FileOutputStream FOS = new FileOutputStream(Result_Sheetpath);
		Wbook_obj.write(FOS);
		FOS.close();
	}
	
	public static void WriteResultToExcel(String Res_Arr [],String Sheetname){
		try{
			String Result_Sheetpath=Driver.LowLeveL_Result_Sheetpath;
			File ResultFile_obj=new File(Result_Sheetpath);
			if (ResultFile_obj.exists()==false){
				GenericFunctions.CreateResultExcel(Sheetname+"_Detailed_Results");
			}
			GenericFunctions.FormatResultExcel(Res_Arr,Sheetname+"_Detailed_Results");
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static String fn_Data(String arg_FieldName) {
		ArrayList<String> TD_AL = Driver.FL;
		int dataCnt = TD_AL.size();
		String xl_FiledValue = null;
		for (int i = 0; i <= dataCnt-1; i++) {
			String xl_FiledName = (String) TD_AL.get(i);
			if (xl_FiledName.equalsIgnoreCase(arg_FieldName)) {
				xl_FiledValue = (String) TD_AL.get(i + 1);
				Logger.info("FiledName "+xl_FiledName+" and FiledValue "+xl_FiledValue);
				break;
			}
		}
		return xl_FiledValue;
	};
		
		
	// *********Function for Current Date
	public static String fn_GetCurrentDate() {
		Date dte = new Date();
		DateFormat df = DateFormat.getDateInstance();
		String strdte = df.format(dte);
		strdte = strdte.replaceAll(":", "_");
		strdte = strdte.replaceAll(",", "_");
		strdte = strdte.replaceAll(" ", "_");
		return strdte;
	}

	// *********Function for Current Date
	public static String fn_GetCurrentTimeStamp() {
		Date dte = new Date();
		DateFormat df = DateFormat.getDateTimeInstance();
		String strdte = df.format(dte);
		strdte = strdte.replaceAll(":", "_");
		strdte = strdte.replaceAll(",", "_");
		strdte = strdte.replaceAll(" ", "_");
		return strdte;
	}
	 
	  public static String Fn_TakeSnapShotAndRetPath(WebDriver WebDriver_Object){
		  String FullSnapShotFilePath = null ;
		  try{
			  /*
			  //window focus
			  if(Driver.browsername.equalsIgnoreCase("ff")){
				if (winCount() == 1) {
					// driverting focus to default window
					Set<String> windows_chk = driver.getWindowHandles();
					for (String window : windows_chk) {
						driver.switchTo().window(window);
					}
					Logger.info("Window focused for screenshot!!");
				}
			  }else if(Driver.browsername.equalsIgnoreCase("ie")){
				String activate = fn_Data("defaultwindow");
				if (activate != null) {
					if (activate.equalsIgnoreCase("activate")) {
						// driverting focus to window
						Set<String> windows_chk = driver.getWindowHandles();
						for (String window : windows_chk) {
							driver.switchTo().window(window);
						}
					}
				}
			  }
			  */
			  String TimeStamp = GenericFunctions.fn_GetCurrentTimeStamp();
			  String FolderPath = Resourse_path.currPrjDirpath +"/Results"+"/Snapshots"+"/SnapshotFolder_"+Resourse_path.DateStamp;
			  File FolderObj=new File(FolderPath);
			  FolderObj.mkdir();
			  FolderPath=FolderObj.getAbsolutePath();
			  String Snap_ScreenName=Driver.ScreenName.replaceAll(" ", "_");
			  FullSnapShotFilePath=FolderPath+"/"+ Driver.HL_RSheetName +"/"+Driver.Testcasename+"/"+Snap_ScreenName+"__"+Driver.KeywordName+"__("+TimeStamp+").png";
			  File DestFile=new File(FullSnapShotFilePath);
			  //take screenshot
			  File SrcFile=((TakesScreenshot)WebDriver_Object).getScreenshotAs(OutputType.FILE);
			  FileUtils.copyFile(SrcFile, DestFile);
		  }catch(Exception e){
			  Logger.warn("Take Screenshot : fail :: error occurs :: using different function to capture image");
			  captureScreen(FullSnapShotFilePath);
		  }
		  return FullSnapShotFilePath;
	   }
    
	//Take screenshot using java function  
	public static void captureScreen(String fileName) {
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle screenRectangle = new Rectangle(screenSize);
			Robot robot = new Robot();
			BufferedImage image = robot.createScreenCapture(screenRectangle);
			ImageIO.write(image, "png", new File(fileName));
			Logger.info("Screenshot captured using java function!!");
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
	} 
	  
	public static void handleMultipleWindows(String windowTitle) {
		Logger.info("Title in xml file - "+windowTitle);
		Set<String> windows = driver.getWindowHandles();
		boolean window_ana = false;
		for (String window : windows) {
			driver.switchTo().window(window);
			if (driver.getTitle().equalsIgnoreCase(windowTitle)) {
				window_ana = true;
				Logger.info("Title of the page after - switching window To: "+ driver.getTitle());
				Logger.info("Desired window is activated!!");
				driver.manage().window().maximize();
				break;
			}
		}
		// switching result-
		if (window_ana != true) {
			Logger.warn("Window is not switched, now trying based on element!!");
			handleMultipleWindowsObjectBased();
		}
	}
	
	public static void handleMultipleWindowsObjectBased() {
		Set<String> windows = driver.getWindowHandles();
		boolean window_ana = false;
		for (String window : windows) {
			driver.switchTo().window(window);
			Logger.info("Window title "+driver.getTitle());
			List<WebElement> fieldobjs = Field_objs(Driver.OR_ObjectName);
			if(fieldobjs.size()>0){
				Logger.info("Desired window is activated!!");
				window_ana = true;
				driver.manage().window().maximize();
				break;
			}
		}
		// switching result-
		if (window_ana != true) {
			Logger.warn("Window is not switched !!");
		}
	}
	

	  
	public static int winCount() throws InterruptedException {
		int winCount = 0;
		try{
			if (driver != null) {
				Thread.sleep(2000);
				winCount = driver.getWindowHandles().size();
				Logger.info("Current WebDriver Win Count is:: " + winCount);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return winCount;
	}
	  
	public static void DrawGraph(String HL_Result_SheetName) {

		try{
			int pass_count = 0;
			int Fail_count = 0;

			FileInputStream chart_file_input = new FileInputStream(new File(Driver.HighLevel_Result_Sheetpath));
			XSSFWorkbook my_workbook = new XSSFWorkbook(chart_file_input);
			XSSFSheet my_sheet = my_workbook.getSheet(HL_Result_SheetName	+ "_High_Level");
			DefaultPieDataset my_pie_chart_data = new DefaultPieDataset();
			
			int rownum = my_sheet.getLastRowNum();
			for (int i = 1; i <= rownum; i++) {
				XSSFRow my_Row = my_sheet.getRow(i);
				XSSFCell my_cell = my_Row.getCell(4);
				String Status = my_cell.getStringCellValue();
				if (Status.equalsIgnoreCase("Passed")) {
					pass_count = pass_count + 1;
				} else if (Status.equalsIgnoreCase("Failed")) {
					Fail_count = Fail_count + 1;
				}
			}
			String Status_p = "Passed";
			String Status_f = "Failed";

			my_pie_chart_data.setValue(Status_p, pass_count);
			my_pie_chart_data.setValue(Status_f, Fail_count);

			JFreeChart myPieChart = ChartFactory.createPieChart(
					"Execution Status", my_pie_chart_data, true, true, false);
			
			PiePlot plot = (PiePlot) myPieChart.getPlot();
			plot.setSectionPaint(Status_f, Color.RED);
			plot.setSectionPaint(Status_p, Color.GREEN);
			plot.setBackgroundPaint(Color.white);
			
			plot.setLabelGenerator(new StandardPieSectionLabelGenerator(
					"{0}{2}", NumberFormat.getNumberInstance(), NumberFormat.getPercentInstance()));
			int width = 500;
			int height = 500;
			float quality = 1;
			
			ByteArrayOutputStream chart_out = new ByteArrayOutputStream();
			ChartUtilities.writeChartAsJPEG(chart_out, quality, myPieChart,width, height);
			
			InputStream feed_chart_to_excel = new ByteArrayInputStream(chart_out.toByteArray());
			
			byte[] bytes = IOUtils.toByteArray(feed_chart_to_excel);
			int my_picture_id = my_workbook.addPicture(bytes,	Workbook.PICTURE_TYPE_JPEG);
			
			feed_chart_to_excel.close();
			chart_out.close();
			XSSFDrawing drawing = my_sheet.createDrawingPatriarch();
			ClientAnchor my_anchor = new XSSFClientAnchor();
			my_anchor.setCol1(7);
			my_anchor.setRow1(2);
			XSSFPicture my_picture = drawing.createPicture(my_anchor,my_picture_id);
			my_picture.resize();
			chart_file_input.close();
			FileOutputStream out = new FileOutputStream(new File(Driver.HighLevel_Result_Sheetpath));
			my_workbook.write(out);
			out.close();
			my_workbook.close();
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void Login(String UNAME, String PASSWORD) {
		exe_rst_status = 2;
		//username
		executeInputOperation("UserName", UNAME);
		//password
		executeInputOperation("Password", PASSWORD);
		//button
		Click("LoginButton");
		exe_rst_status = 1;
	}
	
	
	public static void fileUpload(String OR_ObjectName, String Path){
		exe_rst_status = 2;
		
		WebElement Object_name = GenericFunctions.Field_obj(OR_ObjectName);
		
		Object_name.sendKeys(Path);
		
		exe_rst_status = 1;
	}
	
	public static void INPUT(String OR_ObjectName, String Value){
		String fl = OR_ObjectName.trim();
		if (fl.contains("robot_")) {
			executeRobotevent(fl);
		} else {
			executeInputOperation(OR_ObjectName, Value);
		}
	}
	
	public static void EnterText(WebElement Object_name, String ValueToEnter) {
		Logger.info("ValueToEnter "+ValueToEnter);
		Object_name.clear();
		Object_name.sendKeys(ValueToEnter);
		Logger.info("Entered value is "+ValueToEnter);
		//validating entered value
		boolean vluecheck1 = validateTextfieldvalue(Object_name,ValueToEnter);
		if(vluecheck1!=true){
			//Filling value again if the value is not entered
			Logger.info("Try - Enter data with action builder");
			Actions builder = new Actions(driver);
			builder.moveToElement(Object_name).sendKeys(ValueToEnter).build().perform();
			boolean vluecheck2 = validateTextfieldvalue(Object_name,ValueToEnter);
			if(vluecheck2!=true){
				Logger.info("Try - Enter data with robot action");
				typecharacter(ValueToEnter);
				//staticwait(1);
				boolean vluecheck3 = validateTextfieldvalue(Object_name,ValueToEnter);
				//validating entered value
				if(vluecheck3!=true){
					Logger.info("Try - Enter data with autoit");
					String exefilepath = Resourse_path.autoItpath+"genericfunctions"+".exe";
					try {
						@SuppressWarnings("unused")
						Process pb= new ProcessBuilder(exefilepath,"EnterValue",ValueToEnter).start();
					} catch (IOException e) {
						e.printStackTrace();
					}
					boolean vluecheck4 = validateTextfieldvalue(Object_name,ValueToEnter);
					if(vluecheck4!=true){
						Logger.info("Value is not entered in text box");
						if (Object_name.isEnabled()==false){
							Logger.info("The required textbox is not enabled therefore "
									+ ValueToEnter + " cannot be entered");
						}
					}
				}
			}
		}
	}
	
	public static void typecharacter(String s){
		try{
			Robot robot = new Robot();
			byte[] bytes = s.getBytes();
			for (byte b : bytes) {
				int code = b;
				// keycode only handles [A-Z] (which is ASCII decimal [65-90])
				if (code > 96 && code < 123)
					code = code - 32;
				robot.delay(40);
				robot.keyPress(code);
				robot.keyRelease(code);
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static boolean validateTextfieldvalue(WebElement ele, String vl){
		exe_rst_status = 2;
		boolean rsult =false;
		String attvalue = ele.getAttribute("value");
		Logger.info("Attribute entered value "+attvalue);
//		Logger.info("Attribute entered value "+attvalue);
		if(attvalue.equalsIgnoreCase(vl)){
			rsult = true;
			exe_rst_status = 1;
		}
		return rsult ; 
	}
	
	public static void SelectCheckbox(WebElement Object_name,String flag){
		 exe_rst_status = 2;
//	     WebElement Object_name=GenericFunctions.Field_obj(OR_ObjectName);
	     boolean currentFlag = Object_name.isSelected();
	    
	     if (flag.equalsIgnoreCase("select") || flag==null || flag.equals("")){
	    	 if (currentFlag==true){
//	    		 Logger.info("The checkbox  "+Object_name+" is already selected");
	    		 Logger.info("The checkbox  "+Object_name+" is already selected");
	    		 exe_rst_status = 1;
	    	 }
	    	 else {
	    		 Object_name.click();
//	    		 Logger.info("Checkbox is selected!!");
	    		 Logger.info("Checkbox is selected!!");
	    		 exe_rst_status = 1;
	    	 }
	     }
	     else if(flag.equalsIgnoreCase("unselect")) {
	    	 if (currentFlag==true) {
	    		 Object_name.click();
//	    		 Logger.info("Checkbox is unselected!!");
	    		 Logger.info("Checkbox is unselected!!");
	    		 exe_rst_status = 1;
	    	 }
	    	 else{
//	    		 Logger.info("The checkbox  "+Object_name+" is already De-Selected");
	    		 Logger.info("The checkbox  "+Object_name+" is already De-Selected");
	    		 exe_rst_status = 1;
	    	 }
	     } 
	}
	
	public static void RadioSelector(WebElement Object_name,String flag){
		 exe_rst_status = 2;
//	     WebElement Object_name=GenericFunctions.Field_obj(OR_ObjectName);
	     boolean currentFlag = Object_name.isSelected();
	    
	     if (flag.equalsIgnoreCase("select") || flag==null || flag.equals("") ){
	    	 if (currentFlag==true){
//	    		 Logger.info("The Radio Button  "+Object_name+" is already selected");
	    		 Logger.info("The Radio Button  "+Object_name+" is already selected");
	    		 exe_rst_status = 1;
	    	 }
	    	 else {
	    		 Object_name.click();
	    		 exe_rst_status = 1;
	    	 }
	     }
	     else if (flag.equalsIgnoreCase("unselect")){
	    	 if (currentFlag==true) {
	    		 Object_name.click();
	    		 exe_rst_status = 1;
	    	 }
	    	 else {
//	    		 Logger.info("The Radio Button  "+Object_name+" is already De-Selected");
	    		 Logger.info("The Radio Button  "+Object_name+" is already De-Selected");
	    		 exe_rst_status = 1;
	    	 }
	     } else{
	    	 Logger.warn("select is not found in excel");
	     }
	}
	
	public static void SelectFromListBox(WebElement Object_name,String ValueToSelect) {
		exe_rst_status = 2;	
		String action = fn_Data("action");
		
		if(action!=null){
			char [] lngh = action.toCharArray();
			if(lngh.length>0){
				if(action.equalsIgnoreCase("javascript_click")){
					Logger.info("javascript click");
					JavascriptExecutor js = null;
		            if (driver instanceof JavascriptExecutor) {
		                js = (JavascriptExecutor)driver;
		            }
					js.executeScript("arguments[0].click()", Object_name);
					exe_rst_status = 1;	
				}
				else if(action.equalsIgnoreCase("sendkeys")){
					Logger.info("sendkeys");
					Object_name.sendKeys("abcd");
					String e_value = fn_Data("value");
					List<WebElement> optionsize = Object_name.findElements(By.tagName("option"));
					int opsize = optionsize.size();
					Logger.info("optionsize "+opsize);
					for(int j=1; j<=opsize; j++){
						try {
							Robot robot = new Robot();
							String ac_vl = Object_name.getAttribute("value");
							Logger.info("dp_vl "+ac_vl);
							if(e_value.equalsIgnoreCase(ac_vl)){
								robot.keyPress(KeyEvent.VK_TAB);
								robot.keyRelease(KeyEvent.VK_TAB);
								break;
							}else{
								robot.keyPress(KeyEvent.VK_DOWN);
								robot.keyRelease(KeyEvent.VK_DOWN);
							}
						} catch (AWTException e) {
							e.printStackTrace();
							Logger.error(e);
						}
					}
					exe_rst_status = 1;
				}
			}
		}else {
			Select Object_NM = new Select(Object_name);
			String value ="";
			try{
				value = Object_NM.getFirstSelectedOption().getText();
				if(value.equalsIgnoreCase(ValueToSelect)){
					Logger.info(ValueToSelect+" Value is already selected in dropdown!!");
					exe_rst_status = 1;
				}else{
					if (!Object_NM.isMultiple()){
						Object_NM.selectByVisibleText(ValueToSelect);
						Logger.info(ValueToSelect+" value selected!!");
						exe_rst_status = 1;
					}else{
						Logger.info(Object_NM+ " is multiselect drop down list.. Use different Action for this object");
						exe_rst_status = 2;	
					}
				}
			}catch(Exception e){
				Logger.warn("Exception handled for dropdown function");
				if (!Object_NM.isMultiple()){
					Object_NM.selectByVisibleText(ValueToSelect);
					Logger.info(ValueToSelect+" value selected!!");
					exe_rst_status = 1;
				}else{
					Logger.info(Object_NM+ " is multiselect drop down list.. Use different Action for this object");
					exe_rst_status = 2;	
				}
			}
			
		}
	}
	
	public static void Click(String OR_ObjectName) {
		exe_rst_status = 2;
		WebElement Object_name = GenericFunctions.Field_obj(OR_ObjectName);
		
		if (Driver.browsername.equalsIgnoreCase("ie")) {
			String operation = fn_Data("operation");
			if(operation!=null){
				if(operation.equalsIgnoreCase("sendkeys")){
					Object_name.sendKeys(Keys.ENTER);
					Logger.info("Sendkeys click performed on " + OR_ObjectName);
				}
				else if(operation.equalsIgnoreCase("javascript_click")){
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].click()", Object_name);
					Logger.info("javascript click perfomed on "+OR_ObjectName);
				}
			}else{
				 Object_name.click();
				 Logger.info("clicked on " + OR_ObjectName);
			}
			exe_rst_status = 1;
		} else if (Driver.browsername.equalsIgnoreCase("ff")) {
			Object_name.click();
			Logger.info("clicked on " + OR_ObjectName);
			exe_rst_status = 1;
		} else {
			Logger.info("Browser name is not mentioned for last open url!!");
		}
	}
	
	public static boolean handleMultipleWindowsforClosingwindow() {
		boolean rs = false;
		String br_window = fn_Data("browser_window");
		
		if(br_window!=null){
			Logger.info("br_window "+br_window);
			boolean window_ana = false;
			if(br_window.equalsIgnoreCase("title")){
				Set<String> windows = driver.getWindowHandles();
				for (String window : windows) {
					driver.switchTo().window(window);
					String obj_title = driver.getTitle();
					Logger.info("Window title "+driver.getTitle());
					String obj_value = fn_Data("obj_value");
					if(obj_value!=null){
						char [] val_len = obj_value.toCharArray();
						if(val_len.length>0){
							if(obj_title.equalsIgnoreCase(obj_value)){
								Logger.info("Desired window is activated!!");
								window_ana = true;
								rs = true;
								break;
							}
						}
					}else{
						Logger.info("obj_value value null in excel");
					}
				}
			} else if(br_window.equalsIgnoreCase("index")){
				Set<String> windows = driver.getWindowHandles();
				String obj_value = fn_Data("obj_value");
				if(obj_value!=null){
					char [] val_len = obj_value.toCharArray();
					if(val_len.length>0){
						if(Integer.parseInt(obj_value)>=windows.size()){
							//not implemented yet
							//need to write logic here to handle window on index based
							rs = true;
						}
					}
				}else{
					Logger.info("obj_value value null in excel");
				}
				
			} else if(br_window.equalsIgnoreCase("url")){
				Set<String> windows = driver.getWindowHandles();
				for (String window : windows) {
					driver.switchTo().window(window);
					String page_url = driver.getCurrentUrl();
					Logger.info("Window url "+page_url);
					String obj_value = fn_Data("obj_value");
					if(obj_value!=null){
						char [] val_len = obj_value.toCharArray();
						if(val_len.length>0){
							if(page_url.equalsIgnoreCase(obj_value)){
								Logger.info("Desired window is activated!!");
								window_ana = true;
								rs = true;
								break;
							}
						}
					}else{
						Logger.info("obj_value value null in excel");
					}
				}
			}else {
				Set<String> windows = driver.getWindowHandles();
				for (String window : windows) {
					driver.switchTo().window(window);
					Logger.info("Window switched to - "+driver.getTitle());
					List<WebElement> fieldobjs = Field_objs(br_window);
					if(fieldobjs.size()>0){
						Logger.info("Desired window is activated!!");
						window_ana = true;
						rs = true;
						break;
					}
				}
			}
			
			// switching result-
			if (window_ana != true) {
				Logger.info("Window is not switched !!");
			}
			
		}
		return rs ;
	}
	
	public static void Close() {
		exe_rst_status = 2;
		String processName = fn_Data("windowApp_name");

		if (processName != null && !processName.isEmpty()) {
			killProcess(processName);
		} else {
			Logger.info("closing window...");
			
			// closing all window
			String closewindow = fn_Data("closewindow");
			if (closewindow != null) {
				if (closewindow.equalsIgnoreCase("closeall")) {
					driver.quit();
					driver=null;
					// return result status
					exe_rst_status = 1;
				}
				
			}else{
				int wincnt = driver.getWindowHandles().size();
				Logger.info("window size " + wincnt);
				if (wincnt == 1) {
					driver.manage().deleteAllCookies();
					driver.close();
					driver = null;
					Logger.info("Window is closed!!");
					exe_rst_status = 1;
				} else {
					boolean vl = handleMultipleWindowsforClosingwindow();
					if (vl != true) {
						// activating top window
						Set<String> wind_prs = driver.getWindowHandles();
						for (String window : wind_prs) {
							driver.switchTo().window(window);
							String url = driver.getCurrentUrl();
							String title = driver.getTitle();
							Logger.info("url " + url + " and title is " + title);
						}
					}

					// close browser instance
					driver.close();
					Logger.info("Window is closed!!");
					int win = driver.getWindowHandles().size();
					if(win==1){
						Set<String> windows = driver.getWindowHandles();
						for (String window : windows) {
							driver.switchTo().window(window);
							break;
						}
					}else if(win>=2){
						Set<String> windows = driver.getWindowHandles();
						for (String window : windows) {
							driver.switchTo().window(window);
						}
					}
					
					String closew = fn_Data("closewindow");
					if(closew!=null){
						if(closew.equalsIgnoreCase("checkagain")){
							// checking widow presence again
							int wincnt2 = driver.getWindowHandles().size();
							if (wincnt2 == 1) {
								Set<String> windows = driver.getWindowHandles();
								// activating top window
								for (String window : windows) {
									driver.switchTo().window(window);
									String url = driver.getCurrentUrl();
									String title = driver.getTitle();
									Logger.info("url " + url + " and title is " + title);
								}
								// checking window session
								// close browser instance
								driver.close();
								Logger.info("Window is closed!!");
							} else if (wincnt2 >= 2) {
								Set<String> windows = driver.getWindowHandles();
								// activating top window
								for (String window : windows) {
									driver.switchTo().window(window);
									String url = driver.getCurrentUrl();
									String title = driver.getTitle();
									Logger.info("url " + url + " and title is " + title);
									Set<String> win_sz = driver.getWindowHandles();
									if (win_sz.size() == 1) {
										break;
									}
									// browser will not get closed where there is only 1
									// window
									// checking window session
									// close browser instance
									driver.close();
									Logger.info("Window is closed!!");
								}
							}
						}
					}
					// return result status
					exe_rst_status = 1;
				}
			}
		}
	}
	
	public static void checkwindowsession(){
		// checking active window session
		String checksession2 = driver.getWindowHandle();
		if (checksession2 != null) {
			driver.switchTo().defaultContent();
			Logger.info("session is not null :: " + checksession2);
		}
	}
	
	public static void quitDriver(){
		Set<String> windows = driver.getWindowHandles();
        Logger.info("window size "+windows.size());
        int windowsize = windows.size();
        if (windowsize == 1) {
            String session = driver.getWindowHandle();
            if(session !=null){
                 driver.quit();
            }else{
                 //activating top window
                 for (String window : windows) {
                      driver.switchTo().window(window);
                      driver.quit();
                      Logger.info("window closed and driver is quit");
                 }
            }
		} else {
			//activating top window
			for (String window : windows) {
				driver.switchTo().window(window);
				String url = driver.getCurrentUrl();
				String title = driver.getTitle();
				Logger.info("url "+url+" and title is "+title);
				driver.quit();
				staticwait(1);
				Logger.info("window closed and driver is quit");
			}
		}
		// quiting the driver instance
		if (driver != null) {
			driver.quit();
		}
		// Assigning null values to driver
		driver = null;
	}
	
	public static void Validate(String OR_ObjectName, String Expval){
		exe_rst_status = 2;
		resultmssg = null;
		String ExpValue = Expval.trim().toLowerCase();
		
		WebElement Object_name=GenericFunctions.Field_obj(OR_ObjectName);
		Logger.info("Exp_value "+Expval);
		
		String ActualValue = Object_name.getText();
		String ActValue = ActualValue.trim().toLowerCase();
		Logger.info("Actual Value"+ ActValue);
		
		if (ActValue.equalsIgnoreCase(ExpValue)){
			exe_rst_status = 1;
			resultmssg = "Exp : "+ExpValue+" and act : "+ActValue+" value matched!!";
		} else {
			resultmssg = "Exp : "+ExpValue+" and act : "+ActValue+" value not matched!!";
			exe_rst_status = 2;
		}	
	}
	
	public static void Validate(String OR_ObjectName,String operator,String VariableType,String Value){
		
		int ReqValue=0;
		int ActValue=0;
		Boolean Flag=false;
		exe_rst_status = 2;
		resultmssg = null ;
		
		WebElement Object_name=GenericFunctions.Field_obj(OR_ObjectName);
		String ActualValue = Object_name.getText();
		Logger.info("Actual value "+ActualValue);
		
		if(VariableType.equalsIgnoreCase("Number")){
			
			ReqValue=Integer.parseInt(Value);
			ActValue=Integer.parseInt(ActualValue);
			
			if(operator.equalsIgnoreCase("Equal to")){
				if (ActValue==ReqValue) {
					Flag=true;
				}
				
			} else if(operator.equalsIgnoreCase("Not Equal to")){
				
				if (ActValue!=ReqValue) {
					Flag=true;
				}
				
			} else if(operator.equalsIgnoreCase("Greater than")){
				
				if (ActValue>ReqValue) {
					Flag=true;
				}
				
			} else if(operator.equalsIgnoreCase("Less than")){
			
				if (ActValue<ReqValue) {
					Flag=true;
				}
			}
			
			if(Flag!=true){
				resultmssg = "Exp : "+ActValue+" should "+operator+" act value : "+ReqValue+" : fail";
				exe_rst_status = 2;
			} else{
			    exe_rst_status = 1;
				resultmssg = "Exp : "+ActValue+" should "+operator+" act value : "+ReqValue+" : Pass";
			}
			
		} else {
			
			if(operator.equalsIgnoreCase("Equal to")){
				if (ActualValue.equalsIgnoreCase(Value)) {
					Flag=true;
				}
			}
			else if(operator.equalsIgnoreCase("Not Equal to")){
				if (!(ActualValue.equalsIgnoreCase(Value))) {
					Flag=true;
				}
			}
			
			if(Flag!=true){
				 resultmssg = "Exp "+Value+" should "+operator+" act value "+ActualValue+" : fail";
				exe_rst_status = 2;
			} else{
			    exe_rst_status = 1;
			    resultmssg = "Exp "+Value+" should "+operator+" act value "+ActualValue+" : Pass";
			}
		}
			
	}
	
	public static void Validate() {
		resultmssg = null ;
		exe_rst_status = 2;
		String PageTitle= driver.getTitle().trim();
		if (PageTitle.equalsIgnoreCase(Driver.ScreenName.trim())){
			exe_rst_status = 1;
			resultmssg = "Exp : "+PageTitle+" and actual value : "+Driver.ScreenName.trim()+" value matched!!";
		} else{
			exe_rst_status = 2;
			resultmssg = "Exp : "+PageTitle+" and actual value : "+Driver.ScreenName.trim()+" value not matched!!";
		}	
		
	}
	
    public static boolean validateElementPresence(String objnm){
    	 boolean rs = false;
    	 resultmssg = null ;
		 exe_rst_status = 2;
    	 List<WebElement> Object_name = GenericFunctions.Field_objs(objnm);
    	 if(Object_name.size()>0){
    		 exe_rst_status = 1;
 			resultmssg = "Element present";
 			rs = true;
    	 }else{
    		 exe_rst_status = 2;
  			resultmssg = "Element not present";
    	 }
    	 return rs;
    }
	
    //return page object attribute from xml
		public static  HashMap<String, String> XMLReading(String OR_Name) {
			String XMLName = Driver.ORXMLName;	
		    String tempDir =Resourse_path.currPrjDirpath +"/ObjectRepositories/"+ Driver.ORNAME_XML +"/"+XMLName+".xml"; 
		    Logger.info("Xml obj_repo path "+tempDir);
			HashMap<String, String> map = new HashMap<String, String>();
			try {
				File File_obj = new File(tempDir);
				if(!File_obj.exists()){
					Logger.info(tempDir+" does not exist!!");
				}
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(File_obj);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName(OR_Name);
				//validating tag in xml
				if(nList.getLength()<1){
					Logger.info("Defined excel object tag is not found in xml!!");
				}
	            //reading xml node
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						NodeList ChildnodeList = eElement.getChildNodes();
						int lenghth = ChildnodeList.getLength();
						for (int j = 0; j < lenghth; j++) {
							Node Cnode = ChildnodeList.item(j);
							if (Cnode.getNodeType() == Node.ELEMENT_NODE) {
								String TagValue = Cnode.getTextContent().trim();
								String TagName = Cnode.getNodeName().trim();
								map.put(TagName.toLowerCase(), TagValue);
							}
						}
					}
				}
			} catch (Exception e) {
	           e.printStackTrace();
	           Logger.error(e);
			}
			return map;
	    }

	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
	//page object for title
	public static void windowInstancehandler(String OR_ObjectName){
		try{
			HashMap<String,String> hp = new HashMap<String,String>();
			hp = XMLReading(OR_ObjectName);
			if(hp.size()>0){
				String data = hp.get("title");
				char [] chlength = data.toCharArray();
				if(chlength.length>0){
					handleMultipleWindows(data);
				}else{
					Logger.info("Title contains null value in xml!!");
					handleMultipleWindowsObjectBased();
				}
			}else{
				Logger.info("Object is not defined in xml!!");
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
	public static ArrayList<String> localObjStorage(){
		ArrayList<String> localobjstorg = new ArrayList<String>();
		// Mapped element
 		localobjstorg.add("id");
 		localobjstorg.add("class");
 		localobjstorg.add("xpath");
 		localobjstorg.add("name");
 		localobjstorg.add("linktext");
 		localobjstorg.add("tagname");
 		localobjstorg.add("css");
 		localobjstorg.add("partiallinktext");
 		
		return localobjstorg;
	}
	
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
	public static HashMap<String, Integer> switchPrpStorage() {
		HashMap<String, Integer> swtich_prp_storage = new HashMap<String, Integer>();
		// Element mapped for executing operation
		swtich_prp_storage.put("id", 1);
		swtich_prp_storage.put("xpath", 2);
		swtich_prp_storage.put("class", 3);
		swtich_prp_storage.put("name", 4);
		swtich_prp_storage.put("linktext", 5);
		swtich_prp_storage.put("tagname", 6);
		swtich_prp_storage.put("css", 7);
		swtich_prp_storage.put("partiallinktext",8);
		
		return swtich_prp_storage;
	}
    
/** @author - brijesh-yadav
    @Function_Name -  Field_obj()
    @Description - It fetch all web object property value from xml, based on object and return the intended 
    webelement for executing the operation.
    @return WebElemet object
    @Created_Date - 18 Dec 2014
    @Modified_Date - 
 */
 	public static WebElement Field_obj(String OR_ObjectName) {
 		driverCheck();
 		WebElement WebEle_obj = null;
 		HashMap<String, String> xml_property_storage = XMLReading(OR_ObjectName);

 		// Storing property name to arraylist
 		ArrayList<String> prpstorage = new ArrayList<String>();

 		for (Map.Entry<String, String> m : xml_property_storage.entrySet()) {
 			prpstorage.add(m.getKey());
 		}

 		ArrayList<String> localobjstorg = localObjStorage();

 		String obj = null;
 		boolean exitprcess = false;
 		boolean mapped = false;
 		for (int i = 0; i < prpstorage.size(); i++) {
 			obj = prpstorage.get(i);
 			// Logger.info("first "+obj);
 			for (int j = 0; j < localobjstorg.size(); j++) {
 				String obj2 = localobjstorg.get(j);
 				if (obj.equalsIgnoreCase(obj2)) {
 					mapped = true;
 					Logger.info(obj+" object successfully mapped!!");
 					String objvalue = xml_property_storage.get(obj.toLowerCase());
 					char[] ch = objvalue.toCharArray();
 					if (ch.length > 0) {
 						Logger.info(obj+" object contains property value !!");
 						exitprcess = true;
 						break;
 					}else{
 						Logger.info(obj+" object contains null property value, so moving to next object..");
 					}
 				}
 			}
 			if (exitprcess != false) {
 				break;
 			}
 		}

 		// object code validation
 		if (exitprcess != false) {

 			String prp_value = xml_property_storage.get(obj);
 			Logger.info("Property name - " + obj);
 			Logger.info("Property value - " + prp_value);

 			HashMap<String, Integer> swtich_prp_storage = switchPrpStorage();

 			// storing property name to hashmap
 			ArrayList<String> switchmapobject = new ArrayList<String>();
 			for (Map.Entry<String, Integer> m : swtich_prp_storage.entrySet()) {
 				switchmapobject.add(m.getKey());
 			}
 			// validating mapping with switch case
 			boolean switchprocess = false;
 			for (int i = 0; i < switchmapobject.size(); i++) {
 				String mpobject = switchmapobject.get(i);
 				if (obj.equalsIgnoreCase(mpobject)) {
 					switchprocess = true;
 					break;
 				}
 			}

 			// swtich case validation
 			if (switchprocess != false) {
 				int switchid = swtich_prp_storage.get(obj);
 //				Logger.info(obj + " element is returned!!");
 				Logger.info("Finding "+obj+" element on webpage..");
 				// Assigning element to webelement
 				switch (switchid) {
 				case 1:
 					WebEle_obj = driver.findElement(By.id(prp_value));
 					break;
 				case 2:
 					WebEle_obj = driver.findElement(By.xpath(prp_value));
 					break;
 				case 3:
 					WebEle_obj = driver.findElement(By.className(prp_value));
 					break;
 				case 4:
 					WebEle_obj = driver.findElement(By.name(prp_value));
 					break;
 				case 5:
 					WebEle_obj = driver.findElement(By.linkText(prp_value));
 					break;
 				case 6:
 					WebEle_obj = driver.findElement(By.tagName(prp_value));
 					break;	
 				case 7:
 					WebEle_obj = driver.findElement(By.cssSelector(prp_value));
 					break;
 				case 8:
 					WebEle_obj = driver.findElement(By.partialLinkText(prp_value));
 					break;	
 				default:
 					Logger.info("Event is not mapped for this operation");
 					break;
 				}
 				Logger.info("Element found!!");
 			} else {
 				Logger.info(obj + " is not mapped in property local storage case!!");
 			}

 		} else {
 			if(xml_property_storage.size()>0){
 				if (mapped != true) {
 					Logger.info("Xml object is not mapped in local object storage!!");
 				}else {
 					if(exitprcess!=true){
 						Logger.info("Xml object is mapped but it does not contains object property value in xml file!!");
 					}
 				}
 			}else{
 				Logger.info("size of the element return by xmlreading is "+prpstorage.size());
 			}
 		}

 		return WebEle_obj;
 	}
 	
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
 	public static List<WebElement> Field_objs(String objectnm) {
 		
 		driverCheck();
 		List<WebElement> fielobjs = null;
 		HashMap<String, String> xml_property_storage = XMLReading(objectnm);

 		// Storing property name to arraylist
 		ArrayList<String> prpstorage = new ArrayList<String>();

// 		Logger.info("prpstorage size "+xml_property_storage.size());
//		Logger.info("prpstorage "+xml_property_storage.get(0));
 		
 		for (Map.Entry<String, String> m : xml_property_storage.entrySet()) {
 			String vl = m.getKey();
 			prpstorage.add(vl);
// 			Logger.info("Key "+vl);
 		}

 		ArrayList<String> localobjstorg = localObjStorage();

 		String obj = null;
 		boolean exitprcess = false;
 		boolean mapped = false;
 		for (int i = 0; i < prpstorage.size(); i++) {
 			obj = prpstorage.get(i);
 			// Logger.info("first "+obj);
 			for (int j = 0; j < localobjstorg.size(); j++) {
 				String obj2 = localobjstorg.get(j);
 				if (obj.equalsIgnoreCase(obj2)) {
 					mapped = true;
 					Logger.info(obj+" object successfully mapped!!");
 					String objvalue = xml_property_storage.get(obj.toLowerCase());
 					char[] ch = objvalue.toCharArray();
 					if (ch.length > 0) {
 						Logger.info(obj+" object contains property value !!");
 						exitprcess = true;
 						break;
 					}else{
						Logger.info(obj+" object contains null property value, so moving to next object..");
 					}
 				}
 			}
 			if (exitprcess != false) {
 				break;
 			}
 		}

 		// object code validation
 		if (exitprcess != false) {

 			String prp_value = xml_property_storage.get(obj);
 			Logger.info("Property name - " + obj);
 			Logger.info("Property value - " + prp_value);

 			HashMap<String, Integer> swtich_prp_storage = switchPrpStorage();

 			// storing property name to hashmap
 			ArrayList<String> switchmapobject = new ArrayList<String>();
 			for (Map.Entry<String, Integer> m : swtich_prp_storage.entrySet()) {
 				switchmapobject.add(m.getKey());
 			}
 			// validating mapping with switch case
 			boolean switchprocess = false;
 			for (int i = 0; i < switchmapobject.size(); i++) {
 				String mpobject = switchmapobject.get(i);
 				if (obj.equalsIgnoreCase(mpobject)) {
 					switchprocess = true;
 					break;
 				}
 			}

 			// swtich case validation
 			if (switchprocess != false) {
 				int switchid = swtich_prp_storage.get(obj);
// 				Logger.info(obj + " element is returned!!");
 				Logger.info("Finding "+obj+" element on webpage..");
 				// Assigning element to webelement
 				switch (switchid) {
 				case 1:
 					fielobjs = driver.findElements(By.id(prp_value));
 					returnPresenceofElement(fielobjs);
 					break;
 				case 2:
 					fielobjs = driver.findElements(By.xpath(prp_value));
 					returnPresenceofElement(fielobjs);
 					break;
 				case 3:
 					fielobjs = driver.findElements(By.className(prp_value));
 					returnPresenceofElement(fielobjs);
 					break;
 				case 4:
 					fielobjs = driver.findElements(By.name(prp_value));
 					returnPresenceofElement(fielobjs);
 					break;
 				case 5:
 					fielobjs = driver.findElements(By.linkText(prp_value));
 					returnPresenceofElement(fielobjs);
 					break;
 				case 6:
 					fielobjs = driver.findElements(By.tagName(prp_value));
 					returnPresenceofElement(fielobjs);
 					break;	
 				case 7:
 					fielobjs = driver.findElements(By.cssSelector(prp_value));
 					returnPresenceofElement(fielobjs);
 				case 8:
 					fielobjs = driver.findElements(By.partialLinkText(prp_value));	
 					returnPresenceofElement(fielobjs);
 					break;	
 				default:
 					Logger.info("Event is not mapped for this operation");
 					break;
 				}
 			} else {
 				Logger.info(obj + " is not mapped in property local storage!!");
 			}
 		} else {
 			if(xml_property_storage.size()>0){
 				if (mapped != true) {
 					Logger.info("Xml object is not mapped in local object storage!!");
 				}else {
 					if(exitprcess!=true){
 						Logger.info("Xml object is mapped but it does not contains object property value in xml file!!");
 					}
 				}
 			}else{
 				Logger.info("size of the element return by xmlreading is "+prpstorage.size());
 			}
 		}
 		return fielobjs;
 	}
 	
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
 	public static boolean dynamicwaitforObject(String objectnm, int time) {
 		driverCheck();
 		boolean fielobjs = false;
 		HashMap<String, String> xml_property_storage = XMLReading(objectnm);

 		// Storing property name to arraylist
 		ArrayList<String> prpstorage = new ArrayList<String>();

 		for (Map.Entry<String, String> m : xml_property_storage.entrySet()) {
 			String vl = m.getKey();
 			prpstorage.add(vl);
 //			Logger.info("Key "+vl);
 		}

 		ArrayList<String> localobjstorg = localObjStorage();

 		String obj = null;
 		boolean exitprcess = false;
 		boolean mapped = false;
 		for (int i = 0; i < prpstorage.size(); i++) {
 			obj = prpstorage.get(i);
 			// Logger.info("first "+obj);
 			for (int j = 0; j < localobjstorg.size(); j++) {
 				String obj2 = localobjstorg.get(j);
 				if (obj.equalsIgnoreCase(obj2)) {
 					mapped = true;
// 					Logger.info(obj+" object successfully mapped!!");
 					String objvalue = xml_property_storage.get(obj.toLowerCase());
 					char[] ch = objvalue.toCharArray();
 					if (ch.length > 0) {
 //						Logger.info(obj+" object contains property value !!");
 						exitprcess = true;
 						break;
 					}else{
	//					Logger.info(obj+" object contains null property value, so moving to next object..");
 					}
 				}
 			}
 			if (exitprcess != false) {
 				break;
 			}
 		}

 		// object code validation
 		if (exitprcess != false) {
 			String prp_value = xml_property_storage.get(obj);
 			HashMap<String, Integer> swtich_prp_storage = switchPrpStorage();
 			// storing property name to hashmap
 			ArrayList<String> switchmapobject = new ArrayList<String>();
 			for (Map.Entry<String, Integer> m : swtich_prp_storage.entrySet()) {
 				switchmapobject.add(m.getKey());
 			}
 			// validating mapping with switch case
 			boolean switchprocess = false;
 			for (int i = 0; i < switchmapobject.size(); i++) {
 				String mpobject = switchmapobject.get(i);
 				if (obj.equalsIgnoreCase(mpobject)) {
 					switchprocess = true;
 					break;
 				}
 			}

 			// swtich case validation
 			if (switchprocess != false) {
 				int switchid = swtich_prp_storage.get(obj);
 				Logger.info("Dynamic wait time for object is "+time+" sec");
 				// Assigning element to webelement
 				switch (switchid) {
 				case 1:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.id(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					break;
 				case 2:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.xpath(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					break;
 				case 3:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.className(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					break;
 				case 4:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.name(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					
 					break;
 				case 5:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.linkText(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					
 					break;
 				case 6:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.tagName(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					
 					break;	
 				case 7:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.cssSelector(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					
 					break;
 					
 				case 8:
 					for(int i=1; i<=time; i++){
 						List<WebElement> ele = driver.findElements(By.cssSelector(prp_value));
 						if(ele.size()>0){
 							Logger.info("Element present");
 							fielobjs = true;
 							break;
 						}else{
 							//staticwait(1);
 							Logger.info("waiting for element to present !!"+i+" sec");
 						}
 					}
 					
 					break;	
 				default:
 					Logger.info("Event is not mapped for this operation");
 					break;
 				}
 			} else {
 				Logger.info(obj + " is not mapped in property local storage!!");
 			}
 			
 			if(fielobjs!=true){
 	 			Logger.info("Element is not present");
 	 		}
 			
 		} else {
 			if(xml_property_storage.size()>0){
 				if (mapped != true) {
 					Logger.info("Xml object is not mapped in local object storage!!");
 				}else {
 					if(exitprcess!=true){
 						Logger.info("Xml object is mapped but it does not contains object property value in xml file!!");
 					}
 				}
 			}else{
 				Logger.info("size of the element return by xmlreading is "+prpstorage.size());
 			}
 		}
 		return fielobjs;
 	}
 	
 	public static void returnPresenceofElement(List<WebElement> ele){
 		if(ele.size()>0){
 			Logger.info("Element found!!");
 		}else {
 			Logger.info("Element not found!!");
 		}
 	}
 	
	public static By returnByObject(String OR_ObjectName){
		driverCheck();
		By object = null;
		
 		HashMap<String, String> xml_property_storage = XMLReading(OR_ObjectName);

 		// Storing property name to arraylist
 		ArrayList<String> prpstorage = new ArrayList<String>();

 		for (Map.Entry<String, String> m : xml_property_storage.entrySet()) {
 			prpstorage.add(m.getKey());
 		}

 		ArrayList<String> localobjstorg = localObjStorage();

 		String obj = null;
 		boolean exitprcess = false;
 		boolean mapped = false;
 		for (int i = 0; i < prpstorage.size(); i++) {
 			obj = prpstorage.get(i);
 			// Logger.info("first "+obj);
 			for (int j = 0; j < localobjstorg.size(); j++) {
 				String obj2 = localobjstorg.get(j);
 				if (obj.equalsIgnoreCase(obj2)) {
 					mapped = true;
 					Logger.info(obj+" object successfully mapped!!");
 					String objvalue = xml_property_storage.get(obj.toLowerCase());
 					char[] ch = objvalue.toCharArray();
 					if (ch.length > 0) {
 						Logger.info(obj+" object contains property value !!");
 						exitprcess = true;
 						break;
 					}else{
 						Logger.info(obj+" object contains null property value, so moving to next object..");
 					}
 				}
 			}
 			if (exitprcess != false) {
 				break;
 			}
 		}

 		// object code validation
 		if (exitprcess != false) {

 			String prp_value = xml_property_storage.get(obj);
 			Logger.info("Property name - " + obj);
 			Logger.info("Property value - " + prp_value);

 			HashMap<String, Integer> swtich_prp_storage = switchPrpStorage();

 			// storing property name to hashmap
 			ArrayList<String> switchmapobject = new ArrayList<String>();
 			for (Map.Entry<String, Integer> m : swtich_prp_storage.entrySet()) {
 				switchmapobject.add(m.getKey());
 			}
 			// validating mapping with switch case
 			boolean switchprocess = false;
 			for (int i = 0; i < switchmapobject.size(); i++) {
 				String mpobject = switchmapobject.get(i);
 				if (obj.equalsIgnoreCase(mpobject)) {
 					switchprocess = true;
 					break;
 				}
 			}

 			// swtich case validation
 			if (switchprocess != false) {
 				int switchid = swtich_prp_storage.get(obj);
 //				Logger.info(obj + " element is returned!!");
 				Logger.info("Finding "+obj+" element on webpage..");
 				// Assigning element to webelement
 				switch (switchid) {
 				case 1:
 					object = By.id(prp_value);
 					break;
 				case 2:
 					object = By.xpath(prp_value);
 					break;
 				case 3:
 					object = By.className(prp_value);
 					break;
 				case 4:
 					object = By.name(prp_value);
 					break;
 				case 5:
 					object = By.linkText(prp_value);
 					break;	
 				case 6:
 					object = By.tagName(prp_value);
 					break;	
 				case 7:
 					object = By.cssSelector(prp_value);
 					break;	
 				case 8:
 					object = By.partialLinkText(prp_value);
 					break;	
 				default:
 					Logger.info("Event is not mapped for this operation");
 					break;
 				}
 				Logger.info("Element found!!");
 			} else {
 				Logger.info(obj + " is not mapped in property local storage!!");
 			}

 		} else {
 			if(xml_property_storage.size()>0){
 				if (mapped != true) {
 					Logger.info("Xml object is not mapped in local object storage!!");
 				}else {
 					if(exitprcess!=true){
 						Logger.info("Xml object is mapped but it does not contains object property value in xml file!!");
 					}
 				}
 			}else{
 				Logger.info("size of the element return by xmlreading is "+prpstorage.size());
 			}
 		}
		
		return object;
	}
	
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
	public static void OpenURL(String URL, String browserName) {
		exe_rst_status = 2;
		boolean openb = false;
		try {
			if (driver == null) {
				if ((browserName.equalsIgnoreCase("ff"))) {
					System.setProperty("webdriver.firefox.bin", "C:\\Users\\gaurav-chhabra\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
					Logger.info("please wait.. browser getting open !!");
					driver = new FirefoxDriver();
					driver.get(URL);
					Thread.sleep(1000 * 30);
					driver.manage().window().maximize();
					//String ExePath = Resourse_path.currPrjDirpath+"\\BrowserDrivers\\Auth.exe";
					//String Username = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,1, 10);
					//String Password = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,1, 12);
					//Logger.info("Username" + Username);
					//Logger.info("Password" + Password);
					//Process pb = new ProcessBuilder(ExePath, "Firefox", "30",Username, Password).start();
					//driver.get(URL);
					//Thread.sleep(1000 * 30);
					//pb.destroy();
					Logger.info("Browser is opened with " + URL);
					//System.setProperty("webdriver.firefox.bin", "C:\\Users\\gaurav-chhabra\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
					//driver = new FirefoxDriver();
					//Thread.sleep(1000 * 90);
					openb = true;
				}else if(browserName.equalsIgnoreCase("htmlUnit")){
					driver = new HtmlUnitDriver();
				    ((HtmlUnitDriver)driver).setJavascriptEnabled(true);
				    openb = true;
				} else if (browserName.equalsIgnoreCase("ie")||browserName == "none") {
					String iedriverserver = Resourse_path.browserDriverpath;
					//System.setProperty("webdriver.ie.driver",iedriverserver);
					System.setProperty(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY,iedriverserver);
					DesiredCapabilities capab = DesiredCapabilities.internetExplorer();
					capab.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true); 
			        capab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			        capab.setCapability("ignoreZoomSetting", true);
			        capab.setCapability("nativeEvents",false);
			        capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					driver = new InternetExplorerDriver(capab);
					driver.manage().deleteAllCookies();
					openb = true;
				} else if (browserName.equalsIgnoreCase("chrome")) { 
					String chromedriverserver = Resourse_path.browserDriverpath+"chromedriver.exe";
					System.setProperty("webdriver.chrome.driver",chromedriverserver);
					driver = new ChromeDriver();
					openb = true;
				} else if (browserName.equalsIgnoreCase("auth_ff")) {
					driver = new FirefoxDriver();
					driver.manage().window().maximize();
					String ExePath = Resourse_path.currPrjDirpath+"\\BrowserDrivers\\Auth.exe";
					String Username = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,1, 10);
					String Password = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,1, 12);
					Logger.info("Username" + Username);
					Logger.info("Password" + Password);
					Process pb = new ProcessBuilder(ExePath, "Firefox", "30",Username, Password).start();
					driver.get(URL);
					Thread.sleep(1000 * 30);
					pb.destroy();
					Logger.info("Browser is opened with " + URL);
				} else {
					Logger.info("Failed: Invalid Browser: "+ browserName);
				}
			} else {
				Logger.info("Navigating to another url..");
				driver.manage().window().maximize();
				driver.navigate().to(URL);
			}
			// open browser
			if (openb == true) {
				Logger.info("Opening Application..");
				driver.manage().window().maximize();
				driver.get(URL);
				driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
				Logger.info("Browser is opened with " + URL);
			}
			exe_rst_status = 1;
		} catch (Exception e) {
			exe_rst_status = 2;
			e.printStackTrace();
			Logger.error(e);
			Driver.exceptionreport("Object should be found","Object not found!!",e.getMessage());
		}
	}
    
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
	public static void executeRobotevent(String event_nm) {
		try {
			exe_rst_status = 2;
			Robot robot = new Robot();
			HashMap<String, Integer> map = new HashMap<String, Integer>();

			map.put("robot_tab", 1);
			map.put("robot_enter", 2);
			map.put("robot_down", 3);
			map.put("robot_right", 4);
			map.put("robot_typecharacter", 5);

			int dataa = map.get(event_nm.toLowerCase());
			String msg = "operation perfomred!!";

			switch (dataa) {
			case 1:
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
				Logger.info(event_nm + " " + msg);
				exe_rst_status = 1;
				break;
			case 2:
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Logger.info(event_nm + " " + msg);
				exe_rst_status = 1;
				break;
			case 3:
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.keyRelease(KeyEvent.VK_DOWN);
				Logger.info(event_nm + " " + msg);
				exe_rst_status = 1;
				break;
			case 4:
				robot.keyPress(KeyEvent.VK_RIGHT);
				robot.keyRelease(KeyEvent.VK_RIGHT);
				Logger.info(event_nm + " " + msg);
				exe_rst_status = 1;
				break;
			default:
				Logger.info("Event is not mapped for this operation");
				exe_rst_status = 2;
				break;
			}
		} catch (Exception e) {
			exe_rst_status = 1;
			e.printStackTrace();
			Logger.error(e);
		}

	}
	
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
	public static void OtherClickOperation(XSSFCell Cell_obj6){
		if(Cell_obj6!=null){
    		String data1 = Cell_obj6.toString();
			Logger.info("col 6 data : "+data1);
			char [] ch = data1.toCharArray();
			if(ch.length > 0){
				String act_name = Cell_obj6.getStringCellValue();
				if(act_name.contains("link")){
					Logger.info("under link operation");
					GenericFunctions.handlelinkOperation("Click",act_name);
				}else if(act_name.contains("autoit_")){
					Logger.info("under autoIt operation");
					GenericFunctions.executeAutoItOperation(act_name);
				}else if(act_name.contains("robot_")){
					Logger.info("under robot operation");
					GenericFunctions.executeRobotevent(act_name);
				}
			}
    	}
	}
	
	/**
	 * @author - brijesh-yadav
	 * @Function_Name -
	 * @Description -
	 * @return
	 * @Created_Date -
	 * @Modified_Date -
	 */
	public static void executeActionBuilderOperation(String event_nm, String element){
		exe_rst_status = 2;
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		Actions builder = new Actions(driver);
		// Action mapping
		map.put("act_single", 1);
		map.put("act_double", 2);
		map.put("act_xy0", 3);
		map.put("act_hover", 4);
		map.put("act_mouseclick", 5);
		map.put("act_cordinatesclick", 6);
		map.put("act_cordinates_mouseclick", 7);
		map.put("act_rightclick", 8);
		
		WebElement Object_name = GenericFunctions.Field_obj(element);
		Point coordinates = Object_name.getLocation();
		int x = coordinates.getX();
		int y = coordinates.getY();
		Robot robot;
		// switching data operation
		int data = map.get(event_nm.toLowerCase());
		String msg = "click "+event_nm + " " +"operation perfomred!!";
		// operation execution
		switch (data) {
		case 1:
			Logger.info(msg);
			builder.moveToElement(Object_name).click().build().perform();
			
			exe_rst_status = 1;
			break;
		case 2:
			Logger.info(msg);
			builder.moveToElement(Object_name).doubleClick().build().perform();
			exe_rst_status = 1;
			break;
		case 3:
			Logger.info(msg);
			builder.moveToElement(Object_name,0, 0).click().build().perform();
			exe_rst_status = 1;
			break;
		case 4:
			Logger.info(msg);
			builder.moveToElement(Object_name).build().perform();
			exe_rst_status = 1;
			break;
		case 5:
			Logger.info(msg);
			try {
				robot = new Robot();
				robot.mouseMove(coordinates.getX()+230,coordinates.getY()+330); 
				robot.mousePress(InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK);
			} catch (AWTException e) {
				e.printStackTrace();
				Logger.error(e);
			}
			exe_rst_status = 1;
			break;	
		case 6:
			Logger.info(msg);
			builder.moveToElement(Object_name,x,y).click().build().perform();
			exe_rst_status = 1;	
			break;
		case 7:
			Logger.info(msg);
			try {
				robot = new Robot();
				String xcord = fn_Data("xcord");
				String ycord = fn_Data("ycord");
				if(xcord !=null && ycord!=null){
					int xcords = Integer.parseInt(xcord);
					int ycords = Integer.parseInt(ycord);
					robot.mouseMove(coordinates.getX()+xcords,coordinates.getY()+ycords); 
					robot.mousePress(InputEvent.BUTTON1_MASK );
					robot.mouseRelease( InputEvent.BUTTON1_MASK);
				}
				else{
					Logger.info("x and y cordinates are null!!");
				}
			} catch (AWTException e) {
				e.printStackTrace();
				Logger.error(e);
			}
			exe_rst_status = 1;	
			break;
		case 8:
			Logger.info(msg);
			builder.contextClick(Object_name).build().perform();
			exe_rst_status = 1;	
			break;
		default:
			Logger.info("Event is not mapped for this operation");
			break;
		}
	}
	
	// Execute action builder class operation
	public static void executeclickOps(String objname,String event_nm) {
		
		exe_rst_status = 2;
		
		if(event_nm.contains("act_")){
			executeActionBuilderOperation(event_nm,objname);
		}
		else {
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			// Action mapping
			map.put("javascript_click", 1);
			map.put("alert_accept", 2);
			map.put("alert_dismiss", 3);
			map.put("conditional_click", 4);
			map.put("autoit_ff_openfile", 5);
			map.put("autoit_ie_openfile", 6);
			map.put("download_file", 7);
			map.put("autoit_ie_savefile_2", 8);
			map.put("autoit", 9);
			map.put("new_autoit", 10);
			map.put("cordinates_mousedouble_click", 11);
			
			// switching data operation
			String oprnm = event_nm.toLowerCase().trim();
			Logger.info("oprnm "+oprnm);
			
			int data = 0;
			if (map.containsKey(oprnm)){
				 data = map.get(oprnm);
			}
			String filename = fn_Data("filename");
			String msg = "click "+ event_nm + " " +"operation perfomred!!";
			// operation execution
			switch (data) {
			case 0:
				Logger.info(oprnm+" key does not match!!");
				exe_rst_status = 2;
				break;
			case 1:
				Logger.info(msg);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				WebElement upload = GenericFunctions.Field_obj(Driver.OR_ObjectName);
				js.executeScript("arguments[0].click()", upload);
				System.out.println("autoit part");
				exe_rst_status = 1;
				String action = fn_Data("action");
				if(action!=null){
					char [] lngh = action.toCharArray();
					Logger.info("action "+ action);
					if(lngh.length>0){
						staticwait(5);
						executeAutoItOperation(action);
						staticwait(5);
					}
				}
				
				break;
			case 2:
				WebElement Object_name = GenericFunctions.Field_obj(objname);
				Logger.info(msg);
				Object_name.click();
			    if(isAlertPresent()==true){
			    	//staticwait(1);
			    	driver.switchTo().alert().accept();
			    }
				exe_rst_status = 1;
				break;
			case 3:
				WebElement Object_name2 = GenericFunctions.Field_obj(objname);
				Logger.info(msg);
				Object_name2.click();
			    if(isAlertPresent()==true){
			    	//staticwait(1);
			    	driver.switchTo().alert().dismiss();
			    }
				exe_rst_status = 1;
				break;
			case 4:
				exe_rst_status = 0;
				Logger.info(msg);
				List<WebElement> ele = Field_objs(objname);
				if(ele.size()>0){
					Click(objname);
					Logger.info("Conditional element is clicked!!");
					exe_rst_status = 1;
				}
				break;
			case 5:
				Logger.info(msg);
				//delte file before open if it exist..
				deleteFile(filename);
				Click(objname);
				staticwait(5);
				executeAutoItOperation(event_nm.toLowerCase());
				staticwait(5);
				exe_rst_status = 1;
				break;
			case 6:
				Logger.info(msg);
				//delte file before open if it exist..
				deleteFile(filename);
				Click(objname);
				staticwait(12);
				executeAutoItOperation(event_nm.toLowerCase());
				staticwait(9);
				exe_rst_status = 1;
				break;
			case 7:
				Logger.info(msg);
				downloadFile();
				break;
			case 8:
				Logger.info(msg);
				//delte file before open if it exist..
				deleteFile(filename);
				Click(objname);
				staticwait(12);
				executeAutoItOperation(event_nm.toLowerCase());
				staticwait(9);
				exe_rst_status = 1;
			case 9:
				Logger.info(msg);
				//delte file before open if it exist..
				deleteFile(filename);
				Click(objname);
				staticwait(12);
				executeAutoItOperation(event_nm.toLowerCase());
				staticwait(9);
				exe_rst_status = 1;	
				break;
			case 10:
				Logger.info(msg);
				//delte file before open if it exist..
				deleteFile(filename);
				Click(objname);
				staticwait(10);
				executeAutoItOperation(event_nm.toLowerCase());
				staticwait(12);
				exe_rst_status = 1;	
				break;	
			case 11:
				Logger.info(msg);
				WebElement Obj_nm = GenericFunctions.Field_obj(objname);
				Point coordinates = Obj_nm.getLocation();
				try {
					Robot robot = new Robot();
					String xcord = fn_Data("xcord");
					String ycord = fn_Data("ycord");
					if(xcord !=null && ycord!=null){
						int xcords = Integer.parseInt(xcord);
						int ycords = Integer.parseInt(ycord);
						robot.mouseMove(coordinates.getX()+xcords,coordinates.getY()+ycords); 
						robot.mousePress(InputEvent.BUTTON1_MASK );
						robot.mouseRelease( InputEvent.BUTTON1_MASK);
						robot.mousePress(InputEvent.BUTTON1_MASK );
						robot.mouseRelease( InputEvent.BUTTON1_MASK);
						exe_rst_status = 1;	
					}
					else{
						Logger.info("x and y cordinates are null!!");
					}
				} catch (AWTException e) {
					e.printStackTrace();
				}
				break;	
				
			default:
				Logger.info("Event is not mapped for this operation");
				break;
			}
		}
	}
	
	// get any value from excel file based on row and column
	public static String getValuefromExcel(String filepath, String sheetname,int row, int column) {
		String colValue = "none";
		try {
			FileInputStream FIS = new FileInputStream(filepath);
			XSSFWorkbook Workbook_obj = new XSSFWorkbook(FIS);
			XSSFSheet sheet_obj = Workbook_obj.getSheet(sheetname);
			XSSFRow row_obj = sheet_obj.getRow(row);
			XSSFCell cell_obj = row_obj.getCell(column);

			if (cell_obj != null) {
				colValue = cell_obj.toString();
			}
			Workbook_obj.close();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
			colValue = "none";
		}
		return colValue;
	}
 
	public static boolean isRowEmptyInExcel(Row row) {
		try {
			for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
				Cell cell = row.getCell(c);
				if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
					return false;
				}
			}
		} catch (Exception e) {
			
		}
		return true;
	}
	
	// Executing wait operation
	public static boolean waitOperation(XSSFCell obj_5, XSSFCell col_6) {
		boolean process_analyzer = false;
		exe_rst_status = 2;
		resultmssg = null ;
		if(col_6!=null){
			boolean celstatus = validateCell(col_6);
			if(celstatus!=false){
				XSSFCell Cell_objname = col_6;
				String keyword_nm = Cell_objname.getStringCellValue();
				// static and dynamic wait operation
				if (keyword_nm.equalsIgnoreCase("dynamicWait")) {
					XSSFCell Cell_objname2 = obj_5;
					if (Cell_objname2 != null) {
						String ObjectName = Cell_objname2.getStringCellValue();
						char[] celllength = ObjectName.toCharArray();
						if (celllength.length > 0) {
							for (int i = 1; i < 120; i++) {
								Logger.info("ObjectName "+ObjectName);
								List<WebElement> Object_name = GenericFunctions.Field_objs(ObjectName);
								Logger.info("Size of object "+ Object_name.size());
								if (Object_name.size() > 0) {
									process_analyzer = true;
									exe_rst_status = 1;
									resultmssg = "Element found !!";
									Logger.info(resultmssg);
									break;
								} else {
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e) {
										e.printStackTrace();
										Logger.error(e);
									}
									if (i % 10 == 0) {
										Logger.info("System is waiting for the element..");
									}
								}
							}
							if(process_analyzer!=true){
								resultmssg = "Element is not found !!";
								exe_rst_status = 2;
								Logger.info(resultmssg);
							}
						} else {
							resultmssg = keyword_nm+ " Cell contains null value!!";
							exe_rst_status = 2;
							Logger.info(resultmssg);
						}
					} else {
						resultmssg = keyword_nm + " Cell contains null value!!";
						exe_rst_status = 2;
						Logger.info(resultmssg);
					}
				}
			}else{
				process_analyzer = true;
				int time = Integer.parseInt(obj_5.toString());
				Logger.info("Waited for the element for "+time+" sec");
				try{
					Logger.info("waiting for the element for "+time+" sec");
					for(int i=time; i>=1; i--){
						Thread.sleep(1000);
						if(i%5==0){
							Logger.info("Time remaining "+i+" sec");
						}
					}
				}catch(Exception e){
					e.printStackTrace();
					Logger.error(e);
				}
				resultmssg = "Waited for the element for "+time+" sec";
				exe_rst_status = 1;
			}
		}else {
			process_analyzer = true;
			int time = Integer.parseInt(obj_5.toString());
			Logger.info("Waited for the element for "+time+" sec");
			try{
				Logger.info("waiting for the element for "+time+" sec");
				for(int i=time; i>=1; i--){
					Thread.sleep(1000);
					if(i%5==0){
						Logger.info("Time remaining "+i+" sec");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				Logger.error(e);
			}
			resultmssg = "Waited for the element for "+time+" sec";
			exe_rst_status = 1;
		}
		return process_analyzer;
	}
	
	public static String returnTotalTime(String start_time, String end_time) {
		String time = null;
		try {
			// String time1 = "16:40:30";
			// String time2 = "16:58:20";
			SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
			Date date1 = format.parse(start_time);
			Date date2 = format.parse(end_time);
			long difference = date2.getTime() - date1.getTime();
			long milliseconds = difference;
            //divide the time
			int seconds = (int) (milliseconds / 1000) % 60;
			int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
			int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
			
//			Logger.info(hours + ":" + minutes + ":" + seconds);
			time = hours + ":" + minutes + ":" + seconds;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
		return time;
	}
	
	public static ArrayList<String> returnArraylistStringCommaSeprated(String data){
		ArrayList<String> arr = new ArrayList<String>();
		try{
			List<String> list = new ArrayList<String>(Arrays.asList(data.split(",")));
			//adding data to arraylist
			for(int i=0; i<list.size(); i++){
				arr.add(list.get(i).trim());
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
		return arr;
	}
	

	
	public static void handleDynamicOperation(String operationname, String subOperationName){
		try{
			
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			// Action mapping
			map.put("click", 1);
			map.put("validate", 2);
			map.put("input", 3);
			
			// switching data operation
			int data = map.get(operationname.toLowerCase());
			String msg = "operation perfomred!!";
			
			switch(data){
			case 1:
				Logger.info(operationname + " " + msg);
				clickDynacmicOperation(subOperationName);
				break;
			case 2:
				Logger.info(operationname + " " + msg);
				validateDynacmicOperation(subOperationName);
				break;
			case 3:
				Logger.info(operationname + " " + msg);
				break;
			case 4:
				Logger.info(operationname + " " + msg);
				break;
			default:
				Logger.info(operationname+" case is not mapped for this operation");
				break;
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void validateDynacmicOperation(String valdateOperationName){
		
		ArrayList<String> arr = new ArrayList<String>();
		arr = GenericFunctions.returnArraylistStringCommaSeprated(Driver.OR_ObjectName);
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		// Action mapping
		map.put("text", 1);
		map.put("text_xpath", 2);
		map.put("text_xpath_add", 3);
		map.put("element", 4);
		
		// switching data operation
		int data = map.get(valdateOperationName.toLowerCase());
		String msg = "operation perfomred!!";
		String finalmsg = valdateOperationName + " " + msg ;
		
		switch(data){
		case 1:
			Logger.info(finalmsg);
			validateTextDynacmicOperationCases(arr);
			break;
		case 2:
			Logger.info(finalmsg);
			validateTextKeyValue(arr);
			break;
		case 3:
			Logger.info(finalmsg);
			break;
		case 4:
			Logger.info(finalmsg);
			break;
		default:
			Logger.info(valdateOperationName+" case is not mapped for this operation");
			break;	
		}
	}
	
	public static void validateTextDynacmicOperationCases(ArrayList<String> list){
		
		int operation_switch = list.size();
		String msg = "operation perfomred!!";
		String finalmsg = operation_switch + " " + msg ;
		
		int rownm = Driver.executing_row;
		String fieldValue_1 = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
		
		switch(operation_switch){
		case 2:
			Logger.info(finalmsg);
			validateTextDynamicOperation2(list,fieldValue_1);
			break;
		case 3:
			Logger.info(finalmsg);
			validateTextDynamicOperation3(list,fieldValue_1);
			break;
		case 4:
			Logger.info(finalmsg);
			break;
		case 5:
			Logger.info(finalmsg);
			break;
		default:
			Logger.info(operation_switch+" case is not mapped for this operation");
			break;	
		}
	}

	public static void validateTextDynamicOperation2(ArrayList<String> list,String expData){
		exe_rst_status = 2;
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		
		By by1 = returnByObject(element_1);
		By by2 = returnByObject(element_2);
		String actData = null ;
		
		WebElement obj1 = driver.findElement(by1);
		List<WebElement> listobj1 = obj1.findElements(by2);
		Logger.info("Size of element "+listobj1.size());
		
		boolean rst =false;
		if(listobj1.size()>0){
			for(int i=0; i<listobj1.size(); i++){
				WebElement obj2 = listobj1.get(i);
				actData = obj2.getText();
				Logger.info("actData "+actData);
				if(actData.equalsIgnoreCase(expData)){
					rst = true;
					Logger.info("Actual data : "+actData+" and "+" expected data"+ expData+" are equal" );
					exe_rst_status = 1;
					break;
				}
			}
		}
		
		if(rst!=true){
			exe_rst_status = 2;
			Logger.info("Actual data : "+actData+" and "+" expected data"+ expData+" are equal" );
		}
	}
	
	public static void validateTextDynamicOperation3(ArrayList<String> list,String expData){
		exe_rst_status = 2;
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		String element_3 = list.get(2);
		
		By by1 = returnByObject(element_1);
		By by2 = returnByObject(element_2);
		By by3 = returnByObject(element_3);
		
		String actData = null ;
		
		WebElement obj1 = driver.findElement(by1);
		List<WebElement> listobj1 = obj1.findElements(by2);
		Logger.info("Size of element "+listobj1.size());
		
		boolean rst =false;
		if(listobj1.size()>0){
			for(int i=0; i<listobj1.size(); i++){
				WebElement obj2 = listobj1.get(i);
				List<WebElement> listobj2 = obj2.findElements(by3);
				if(listobj2.size()>0){
					for(int loop2=0; loop2<listobj2.size(); loop2++){
						WebElement obj3 = listobj2.get(loop2);
						actData = obj3.getText();
						Logger.info("actData "+actData);
						if(actData.equalsIgnoreCase(expData)){
							rst = true;
							Logger.info("Actual data : "+actData+" and "+" expected data"+ expData+" are equal" );
							exe_rst_status = 1;
							break;
						}
					}
				}
				if(rst!=false){
					break;
				}
			}
		}
		
		if(rst!=true){
			exe_rst_status = 2;
			Logger.info("Actual data : "+actData+" and "+" expected data"+ expData+" are equal" );
		}
		
	}
	
	public static void validateTextKeyValue(ArrayList<String> list){
		exe_rst_status = 2;
		int rownm = Driver.executing_row;
		String keyName = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 7);
		String value  = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
		
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		
		HashMap<String, String> value1 = XMLReading(element_1);
		String xpath1 = value1.get("xpath");
        
        HashMap<String, String> value2 = XMLReading(element_2);
        String xpath2 = value2.get("xpath");
        boolean keyStatus =false;
        boolean valueStatus =false;
        for(int i=1; i<60; i++){
        	int j = i ;
        	String finalelement = xpath1+j+xpath2;
        	List<WebElement> listele = driver.findElements(By.xpath(finalelement));
        	if(listele.size()>0){
        		String eledata = driver.findElement(By.xpath(finalelement)).getText();
        		Logger.info("keyName "+eledata);
        		if(eledata.equalsIgnoreCase(keyName)){
        			keyStatus = true;
        			int add_1 = j+1;
        			String finalelement2 = xpath1+add_1+xpath2;
                	List<WebElement> listele2 = driver.findElements(By.xpath(finalelement2));
                	if(listele2.size()>0){
                		String eledata2 = driver.findElement(By.xpath(finalelement2)).getText();
                		Logger.info("value "+eledata2);
                		if(eledata2.equalsIgnoreCase(value)){
                			valueStatus = true;
                			Logger.info("keyName "+keyName +" and keyName value "+value+" is matched !!");
                			exe_rst_status = 1;
                			break;
                		}
                	}
                	break;
        		}
        	}
        	if (i % 10 == 0) {
				Logger.info("System is searching for the element..");
			}
        }
        
        if(keyStatus!=true){
        	exe_rst_status = 2;
        	Logger.info("keyName "+keyName +" is not found!!");
        }else{
        	if(valueStatus!=true){
        		exe_rst_status = 2;
        		Logger.info("keyName value "+value +" is not matched!!");
        	}
        }
	}
	
	public static void clickDynacmicOperation(String valdateOperationName){
		
		ArrayList<String> arr = new ArrayList<String>();
		arr = GenericFunctions.returnArraylistStringCommaSeprated(Driver.OR_ObjectName);
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		// Action mapping
		map.put("textbased", 1);
		map.put("hover_textbased", 2);
		
		// switching data operation
		int data = map.get(valdateOperationName.toLowerCase());
		String msg = "operation perfomred!!";
		String finalmsg = valdateOperationName + " " + msg ;
		
		switch(data){
		case 1:
			Logger.info(finalmsg);
			clickTextbasedDynacmicOperationCases(arr);
			break;
		case 2:
			Logger.info(finalmsg);
			hoverTextbasedDynacmicOperationCases(arr);
			break;
		case 3:
			Logger.info(finalmsg);
			hoverTextbasedDynacmicOperationCases(arr);
			break;	
		default:
			Logger.info(valdateOperationName+" case is not mapped for this operation");
			break;	
		}
	}
	
	public static void clickTextbasedDynacmicOperationCases(ArrayList<String> list){
		
		int operation_switch = list.size();
		String msg = "operation perfomred!!";
		String finalmsg = operation_switch + " " + msg ;
		int rownm = Driver.executing_row;
		
		String fieldValue_1 = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
		
		switch(operation_switch){
		case 2:
			Logger.info(finalmsg);
			ClickTextbasedDynamicOperation2(list,fieldValue_1);
			break;
		case 3:
			Logger.info(finalmsg);
			ClickTextbasedDynamicOperation3(list,fieldValue_1);
			break;
		case 4:
			Logger.info(finalmsg);
			ClickTextbasedDynamicOperation4(list,fieldValue_1);
			break;
		case 5:
			Logger.info(finalmsg);
			break;
		default:
			Logger.info(operation_switch+" case is not mapped for this operation");
			break;	
		}
	}

	public static void hoverTextbasedDynacmicOperationCases(ArrayList<String> list){
		
		int operation_switch = list.size();
		String msg = "operation perfomred!!";
		String finalmsg = operation_switch + " " + msg ;
		int rownm = Driver.executing_row;
		
		String fieldValue_1 = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
		
		switch(operation_switch){
		case 2:
			Logger.info(finalmsg);
			ClickTextbasedDynamicOperation2(list,fieldValue_1);
			break;
		case 3:
			Logger.info(finalmsg);
			ClickTextbasedDynamicOperation3(list,fieldValue_1);
			break;
		case 4:
			Logger.info(finalmsg);
			hoverTextbasedDynamicOperation4(list,fieldValue_1);
			break;
		case 5:
			Logger.info(finalmsg);
			break;
		default:
			Logger.info(operation_switch+" case is not mapped for this operation");
			break;	
		}
	}

	
	public static void ClickTextbasedDynamicOperation2(ArrayList<String> list,String expData){
		exe_rst_status = 2;
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		
		By by1 = returnByObject(element_1);
		By by2 = returnByObject(element_2);
		
		String actData = null ;
		
		WebElement obj1 = driver.findElement(by1);
		List<WebElement> listobj1 = obj1.findElements(by2);
		Logger.info("Size of element "+listobj1.size());
		
		boolean rst =false;
		if(listobj1.size()>0){
			for(int i=0; i<listobj1.size(); i++){
				WebElement obj2 = listobj1.get(i);
				actData = obj2.getText();
				Logger.info("actData "+actData);
				if(actData.equalsIgnoreCase(expData)){
					rst = true;
					obj2.click();
					exe_rst_status = 1;
					break;
				}
				
			}
		}
		
		if(rst!=true){
			exe_rst_status = 2;
			Logger.info("expData "+expData+"is not found !!" );
		}
	}
	
	public static void ClickTextbasedDynamicOperation3(ArrayList<String> list,String expData){
		exe_rst_status = 2;
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		String element_3 = list.get(2);
		
		By by1 = returnByObject(element_1);
		By by2 = returnByObject(element_2);
		By by3 = returnByObject(element_3);
		
		String actData = null ;
		
		WebElement obj1 = driver.findElement(by1);
		List<WebElement> listobj1 = obj1.findElements(by2);
		Logger.info("Size of element "+listobj1.size());
		
		boolean rst =false;
		if(listobj1.size()>0){
			for(int i=0; i<listobj1.size(); i++){
				WebElement obj2 = listobj1.get(i);
				List<WebElement> listobj2 = obj2.findElements(by3);
				if(listobj2.size()>0){
					for(int loop2=0; loop2<listobj2.size(); loop2++){
						WebElement obj3 = listobj2.get(loop2);
						actData = obj3.getText();
						Logger.info("actData "+actData);
						if(actData.equalsIgnoreCase(expData)){
							rst = true;
							obj3.click();
							exe_rst_status = 1;
							break;
						}
					}
				}
				if(rst!=false){
					break;
				}
			}
		}
		
		if(rst!=true){
			Logger.info("expData "+expData+"is not found !!");
			exe_rst_status = 2;
		}
	}
	
	public static void ClickTextbasedDynamicOperation4(ArrayList<String> list,String expData){
		exe_rst_status = 2;
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		String element_3 = list.get(2);
		String element_4 = list.get(3);
		
		By by1 = returnByObject(element_1);
		By by2 = returnByObject(element_2);
		By by3 = returnByObject(element_3);
		By by4 = returnByObject(element_4);
		
		String actData = null ;
		
		WebElement obj1 = driver.findElement(by1);
		List<WebElement> listobj1 = obj1.findElements(by2);
		Logger.info("Size of element "+listobj1.size());
		
		boolean rst =false;
		if(listobj1.size()>0){
			for(int i=0; i<listobj1.size(); i++){
				WebElement obj2 = listobj1.get(i);
				List<WebElement> listobj2 = obj2.findElements(by3);
				if(listobj2.size()>0){
					for(int loop2=0; loop2<listobj2.size(); loop2++){
						WebElement obj3 = listobj2.get(loop2);
						List<WebElement> listobj3 = obj3.findElements(by4);
						if(listobj3.size() > 0){
							for(int loop3=0; loop3<listobj3.size(); loop3++){
								WebElement obj4 = listobj3.get(loop3);
								actData = obj4.getText();
								Logger.info("actData "+actData);
								if(actData.equalsIgnoreCase(expData)){
									rst = true;
									obj4.click();
									exe_rst_status = 1;
									break;
								}
							}
						}
						if(rst!=false){
							break;
						}
					}
				}
				if(rst!=false){
					break;
				}
			}
		}
		
		if(rst!=true){
			exe_rst_status = 2;
			Logger.info("expData "+expData+"is not found !!" );
		}
	}

	public static void hoverTextbasedDynamicOperation4(ArrayList<String> list,String expData){
		exe_rst_status = 2;
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		String element_3 = list.get(2);
		String element_4 = list.get(3);
		
		By by1 = returnByObject(element_1);
		By by2 = returnByObject(element_2);
		By by3 = returnByObject(element_3);
		By by4 = returnByObject(element_4);
		
		String actData = null ;
		
		WebElement obj1 = driver.findElement(by1);
		List<WebElement> listobj1 = obj1.findElements(by2);
		Logger.info("Size of element "+listobj1.size());
		
		boolean rst =false;
		if(listobj1.size()>0){
			for(int i=0; i<listobj1.size(); i++){
				WebElement obj2 = listobj1.get(i);
				List<WebElement> listobj2 = obj2.findElements(by3);
				if(listobj2.size()>0){
					for(int loop2=0; loop2<listobj2.size(); loop2++){
						WebElement obj3 = listobj2.get(loop2);
						List<WebElement> listobj3 = obj3.findElements(by4);
						if(listobj3.size() > 0){
							for(int loop3=0; loop3<listobj3.size(); loop3++){
								WebElement obj4 = listobj3.get(loop3);
								actData = obj4.getText();
								Logger.info("actData "+actData);
								if(actData.equalsIgnoreCase(expData)){
									rst = true;
									Actions builder = new Actions(driver);
									builder.moveToElement(obj4).click().build().perform();
									exe_rst_status = 1;
									break;
								}
							}
						}
						if(rst!=false){
							break;
						}
					}
				}
				if(rst!=false){
					break;
				}
			}
		}
		
		if(rst!=true){
			exe_rst_status = 2;
			Logger.info("expData "+expData+" is not found !!" );
		}
	}

	
	public static void ClickTextbasedDynamicOperation5(ArrayList<String> list,String expData) {
		exe_rst_status = 2;
		String element_1 = list.get(0);
		String element_2 = list.get(1);
		String element_3 = list.get(2);
		String element_4 = list.get(3);
		String element_5 = list.get(4);

		By by1 = returnByObject(element_1);
		By by2 = returnByObject(element_2);
		By by3 = returnByObject(element_3);
		By by4 = returnByObject(element_4);
		By by5 = returnByObject(element_5);

		String actData = null;

		WebElement obj1 = driver.findElement(by1);
		List<WebElement> listobj1 = obj1.findElements(by2);
		Logger.info("Size of element " + listobj1.size());

		boolean rst = false;
		if (listobj1.size() > 0) {
			for (int i = 0; i < listobj1.size(); i++) {
				WebElement obj2 = listobj1.get(i);
				List<WebElement> listobj2 = obj2.findElements(by3);
				if (listobj2.size() > 0) {
					for (int loop2 = 0; loop2 < listobj2.size(); loop2++) {
						WebElement obj3 = listobj2.get(loop2);
						List<WebElement> listobj3 = obj3.findElements(by4);
						if (listobj3.size() > 0) {
							for (int loop3 = 0; loop3 < listobj3.size(); loop3++) {
								WebElement obj4 = listobj3.get(loop3);
								List<WebElement> listobj4 = obj4
										.findElements(by5);
								if (listobj4.size() > 0) {
									for (int loop4 = 0; loop4 < listobj4.size(); loop4++) {
										WebElement obj5 = listobj3.get(loop3);
										actData = obj5.getText();
										Logger.info("actData " + actData);
										if (actData.equalsIgnoreCase(expData)) {
											rst = true;
											obj5.click();
											exe_rst_status = 1;
											break;
										}
									}
								}
								if (rst != false) {
									break;
								}
							}
						}
						if (rst != false) {
							break;
						}
					}
				}
				if (rst != false) {
					break;
				}
			}
		}

		if (rst != true) {
			exe_rst_status = 2;
			Logger.info("expData " + expData + "is not found !!");
		}
	}
	
	public static void handlelinkOperation(String action, String operationname){
		
		int rownm = Driver.executing_row;
		String lnkname = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
		
		if(action.equalsIgnoreCase("click")){
			clickLinkOperation(operationname,lnkname);
		}
		else if(action.equalsIgnoreCase("validate")){
			validateLinkOperation(lnkname);
		}
	}
	
	public static void clickLinkOperation(String oprnamekey, String lnkname){
		
		exe_rst_status = 2;
		
		By obj = null ;
		
		if(oprnamekey.equalsIgnoreCase("link")){
			obj = By.linkText(lnkname);
		}
		else if(oprnamekey.equalsIgnoreCase("partiallink")){
			obj = By.partialLinkText(lnkname);
		}
		
		boolean status = false;
		
		List<WebElement> ele = driver.findElements(obj);
		if(ele.size()>0){
			if(oprnamekey.equalsIgnoreCase("link")){
				driver.findElement(By.linkText(lnkname)).click();
				status = true;
				exe_rst_status = 1;
			}
			else if(oprnamekey.equalsIgnoreCase("partiallink")){
				driver.findElement(By.partialLinkText(lnkname)).click();
				status = true;
				exe_rst_status = 1;
			}
		}
		//dynamic search lnk object and click it
		if(status != true){
			List<WebElement> ele1 = driver.findElements(By.tagName("a"));
			if(ele1.size()>0){
				for(int i=0; i<ele1.size(); i++){
					WebElement ele2 = ele1.get(i);
					List<WebElement> listele2 = ele2.findElements(By.tagName("span"));
					if(listele2.size()>0){
						for(int j=0; j<listele2.size();j++){
							WebElement ele3 = listele2.get(j);
							String actlnktxt = ele3.getText();
							if(actlnktxt.equalsIgnoreCase(lnkname)){
								ele3.click();
								Logger.info("link object "+lnkname+" found");
								status = true;
								exe_rst_status = 1;
								break;
							}
						}
					}
					if(status != false){
						break;
					}
				}
			}
		}
		
		//final link click status
		if(status!=true){
			Logger.info("Failed : link object "+lnkname+" not found !!");
			exe_rst_status = 2;
		}
		
	}
	
	public static void validateLinkOperation(String lnkname){
		
		exe_rst_status = 2;
		boolean status = false;
		
		List<WebElement> ele = driver.findElements(By.tagName("a"));
		if(ele.size()>0){
			for(int i=0; i<ele.size(); i++){
				WebElement ele2 = ele.get(i);
				String lnkdata = ele2.getText();
				if(lnkdata.equalsIgnoreCase(lnkname)){
					Logger.info("link match");
					exe_rst_status = 1;
					status = true;
					break;
				}
			}
		}
		//dynamic search lnk object and click it
		if(status != true){
			List<WebElement> ele1 = driver.findElements(By.tagName("a"));
			if(ele1.size()>0){
				for(int i=0; i<ele1.size(); i++){
					WebElement ele2 = ele1.get(i);
					List<WebElement> listele2 = ele2.findElements(By.tagName("span"));
					if(listele2.size()>0){
						for(int j=0; j<listele2.size();j++){
							WebElement ele3 = listele2.get(j);
							String actlnktxt = ele3.getText();
							if(actlnktxt.equalsIgnoreCase(lnkname)){
								Logger.info("link object "+lnkname+" found");
								status = true;
								exe_rst_status = 1;
								break;
							}
						}
					}
					if(status != false){
						break;
					}
				}
			}
		}
		
		//final link click status
		if(status!=true){
			Logger.info("Failed : link object "+lnkname+" not found !!");
			exe_rst_status = 2;
		}
		
	}
	
	public static int returnCurrentExecutionResultStatus(){
		return exe_rst_status;
	}
	
	public static void executeAutoItOperation(String OperationName){
		exe_rst_status = 2;
		try{
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			// Action mapping
			map.put("autoit_ff_fileUpload", 1);
			map.put("autoit_ff_openfile", 2);
			map.put("autoit_ie_openfile", 3);
			map.put("autoit_ie_savefile_2", 4);
			map.put("autoit", 5);
			map.put("new_autoit", 6);
			
			int data = 0;
			if (map.containsKey(OperationName)){
				 data = map.get(OperationName);
			}
			String msg = "operation perfomred!!";
			String initialized = "Autoit part initialized";
			String finalmsg = "autoit "+OperationName + " " + msg ;
			
			String autoitfilepath = Resourse_path.autoItpath+OperationName+".exe";
			Logger.info("autoitfilepath "+autoitfilepath);
			int rownm = Driver.executing_row;
			
			@SuppressWarnings("unused")
			Process pb ;
			
			switch(data){
			case 0:
				Logger.info(OperationName+" key does not match!!");
				exe_rst_status = 2;
				break;
			case 1:
				Logger.info(initialized);
				String path = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
				String filepath = Resourse_path.currPrjDirpath+File.separator+path;
				pb= new ProcessBuilder(autoitfilepath,filepath).start();
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;
			case 2:
				Logger.info(initialized);
				String windowtitle = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
				pb= new ProcessBuilder(autoitfilepath,windowtitle).start();
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;
			case 3:
				Logger.info(initialized);
				pb= new ProcessBuilder(autoitfilepath).start();
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;	
			case 4:
				Logger.info(initialized);
				pb= new ProcessBuilder(autoitfilepath).start();
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;		
			case 5:
				Logger.info(initialized);
				String exefilename = getValuefromExcel(Driver.TestData_Sheetpath, Driver.DriverSheetname,rownm, 8);
				String exefilepath = Resourse_path.autoItpath+exefilename+".exe";
				pb= new ProcessBuilder(exefilepath).start();
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;
			case 6:
				Logger.info(initialized);
				executeAutoItParameterBased();
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;		
			default:
				Logger.info(OperationName+" case is not mapped for this operation");
				exe_rst_status = 2;
				break;	
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	
	}
	
	public static void executeAutoItParameterBased(){
		try{
			String flnm = fn_Data("filename");
			String parameter = fn_Data("parameter");
			String [] arr = parameter.split(",");
			int par_len = arr.length;
			
			//defined null variables
			String var1 = null ;
			String var2 = null ;
			String var3 = null ;
			String var4 = null ;
			String var5 = null ;
			
			String fl = null ;
			if(flnm!=null){
				fl = flnm;
			}else{
				fl = "genericfunctions";
			}
			
			@SuppressWarnings("unused")
			Process pb ;
			String exefilepath = Resourse_path.autoItpath+fl+".exe";
			
			switch(par_len){
			case 1:
				var1 = arr[0];
				pb= new ProcessBuilder(exefilepath,var1).start();
				break;
				
			case 2:
				var1 = arr[0];
				var2 = arr[1];
				String path = fn_Data("path");
				if(path!=null){
					if(path.equalsIgnoreCase("default")){
						createDownloadFolderpath();
						var2 = Resourse_path.comp_downloadfoder+var2;
					}
				}
				pb= new ProcessBuilder(exefilepath,var1,var2).start();
				break;
				
			case 3:
				var1 = arr[0];
				var2 = arr[1];
				var3 = arr[2];
				pb= new ProcessBuilder(exefilepath,var1,var2,var3).start();
				break;
				
			case 4:
				var1 = arr[0];
				var2 = arr[1];
				var3 = arr[2];
				var4 = arr[3];
				pb= new ProcessBuilder(exefilepath,var1,var2,var3,var4).start();
				break;
				
			case 5:
				var1 = arr[0];
				var2 = arr[1];
				var3 = arr[2];
				var4 = arr[3];
				var5 = arr[4];
				pb= new ProcessBuilder(exefilepath,var1,var2,var3,var4,var5).start();
				break;	
				
			default:
				Logger.info(par_len+" case is not mapped for this operation");
				exe_rst_status = 2;
				break;	
			}
			resultmssg=var1+" Operation performed successfully!!";	
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static boolean validateCell(XSSFCell Cell_obj){
		boolean cellstatus = false;
		try{
			if(Cell_obj!=null){
		   		String data1 = Cell_obj.toString();
				Logger.info("col 6 data ; "+data1);
				char [] ch = data1.toCharArray();
				if(ch.length>0){
					cellstatus=true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
		return cellstatus;
	}
	
	public static boolean isAlertPresent() {
		boolean presentFlag = false;
		try {
			@SuppressWarnings("unused")
			Alert alert = driver.switchTo().alert();
			// Alert present
			presentFlag = true;
		} catch (NoAlertPresentException ex) {
//			ex.printStackTrace();
			Logger.info("Alert is not present");
		}
		return presentFlag;

	}
	
	public static void staticwait(int time){
		try{
			Logger.info("Total static wait time "+time);
			for(int i=time; i>0; i--){
				Thread.sleep(1000);
				if(i%5==0){
					Logger.info("System waiting for the element.."+i+" sec to appear!!");
				}
			}
		}catch(Exception e){
			e.getMessage();
		}
	}
	
	  public static int returnFinalrow(XSSFSheet my_sheet){
		  int row = 0;
		  try{
			  int totalrow = my_sheet.getLastRowNum();
				Logger.info("totalrow "+totalrow+1);
				int introw = 0;
				for(int i=0; i<totalrow; i++){
					boolean rw = isRowEmptyInExcel(my_sheet.getRow(i));
					if(rw!=true){
						introw++;
					}
				}
				row = introw;
				Logger.info("Total final row "+row);
		  }catch(Exception e){
			  e.printStackTrace();
			  Logger.error(e);
		  }
		  return row;
	  }
	  
	public static void executeInputOperation(String OR_ObjectName, String Value) {
		
		exe_rst_status = 2;
		WebElement Object_name = GenericFunctions.Field_obj(OR_ObjectName);

		String ObjectType = Object_name.getAttribute("Type");
		String ObjectTag = Object_name.getTagName();

//		Logger.info("ObjectTag " + ObjectTag + " and ObjectType "	+ ObjectType);
		Logger.info("ObjectTag " + ObjectTag + " and ObjectType "	+ ObjectType);

		if (ObjectType == null) {
			ObjectType = Object_name.getAttribute("type");
			if(ObjectType==null){
//				Logger.info("Object Type is null");
				Logger.info("Object Type is null");
				ObjectType = "Cannot find";
				exe_rst_status = 2;
			}
		}

		if (ObjectTag == null) {
//			Logger.info("Object Tag is null");
			Logger.info("Object Tag is null");
			ObjectTag = "Cannot find";
			exe_rst_status = 2;
		}
		
		//select value from combo box
		if ((ObjectTag.equalsIgnoreCase("select")) || (ObjectTag.equalsIgnoreCase("option"))) {
//			Logger.info("select combobox operation ");
			Logger.info("select combobox operation ");
			SelectFromListBox(Object_name, Value);
		} 
		
		//select checkbox
		else if (ObjectType.equalsIgnoreCase("checkbox")) {
//			Logger.info("select checkbox operation ");
			Logger.info("select checkbox operation ");
			SelectCheckbox(Object_name, Value);
		}
		//select checkbox
		else if(ObjectType.equalsIgnoreCase("checkbox") && (ObjectTag.equalsIgnoreCase("input"))){
//			Logger.info("select checkbox operation ");
			Logger.info("select checkbox operation ");
			SelectCheckbox(Object_name, Value);
		}
		//Text input field
		else if (ObjectType.equalsIgnoreCase("text") && ObjectTag.equalsIgnoreCase("input") || ObjectType.equalsIgnoreCase("text")) {
//			Logger.info("Enter data operation");
			Logger.info("Enter data operation");
			EnterText(Object_name, Value);
		}
		//Text input field
		else if (ObjectType.equalsIgnoreCase("password") && ObjectTag.equalsIgnoreCase("input") || ObjectType.equalsIgnoreCase("password")) {
//			Logger.info("Enter data operation");
			Logger.info("Enter data operation");
			EnterText(Object_name, Value);
		} 
		//select radio
		else if (ObjectType.equalsIgnoreCase("radio")) {
//			Logger.info("Radio operation ");
			Logger.info("Radio operation ");
			RadioSelector(Object_name, Value);
		} 
		//select radio
		else if (ObjectType.equalsIgnoreCase("radio") && ObjectTag.equalsIgnoreCase("input")) {
//			Logger.info("Radio operation ");
			Logger.info("Radio operation ");
			RadioSelector(Object_name, Value);
		} 
		//default message
		else {
//			Logger.info(" Object type and tag has not been built yet");
			Logger.info(" Object type and tag has not been built yet");
			exe_rst_status = 2;
		}
		
	  }
	
	public static void clickCondition(String OR_ObjectName, XSSFCell Cell_obj6){
		char [] ch = OR_ObjectName.toCharArray();
		if(ch.length>0){
			Logger.info("Under single object handle operation!!");
			if(Cell_obj6!=null){
	    		String col6 = Cell_obj6.toString();
				Logger.info("col 6 data : "+col6+" and col 5 data : "+OR_ObjectName);
				char [] chcol6 = col6.toCharArray();
				if(chcol6.length > 0){
					Logger.info("Click case operation !!");
					executeclickOps(OR_ObjectName,col6 );
				}else {
					Logger.info("Normal click operation !!");
					Click(OR_ObjectName);
				}
	    	}else {
				Logger.info("Normal click operation !!");
				Click(OR_ObjectName);
			}
		}else {
			Logger.info("Other click operation !!");
			OtherClickOperation(Cell_obj6);
		}
	}
	
	public static void killProcess(String Name) {
		exe_rst_status = 2;
		try {
			WinNT winNT = (WinNT) Native.loadLibrary(WinNT.class,W32APIOptions.UNICODE_OPTIONS);
			WinNT.HANDLE snapshot = winNT.CreateToolhelp32Snapshot(Tlhelp32.TH32CS_SNAPPROCESS, new WinDef.DWORD(0));
			Tlhelp32.PROCESSENTRY32.ByReference processEntry = new Tlhelp32.PROCESSENTRY32.ByReference();
			String pname = Name+".exe";
			while (winNT.Process32Next(snapshot, processEntry)) {
				DWORD processid = processEntry.th32ProcessID;
				String processname = Native.toString(processEntry.szExeFile);
                // kiling process
				if (processname.equalsIgnoreCase(pname)) {
					Logger.info("processid " + processid+ " and processname " + processname);
					Runtime rt = Runtime.getRuntime();
					@SuppressWarnings("unused")
					Process proc = rt.exec("taskkill /F /IM " + processname);
					exe_rst_status = 1;
					Logger.info(processname+" Process killed");
				}
			}
			winNT.CloseHandle(snapshot);
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void validatekeyValueInExcel(String filepath, String sheetname, String key, String value){
		exe_rst_status = 2;
		try{
			if(filepath!=null){
				File file = new File(filepath);
				if(file.exists()){
					FileInputStream FIS = new FileInputStream(filepath);
					XSSFWorkbook Workbook_obj = new XSSFWorkbook(FIS);
					XSSFSheet sheet_obj = Workbook_obj.getSheet(sheetname);
					//total row return
					int finalrow = returnFinalrow(sheet_obj);
					boolean keys = false;
					boolean values =false;
					//logic
					for(int i=1; i<=finalrow; i++){
						XSSFRow row_obj = sheet_obj.getRow(i);
						if(row_obj!=null){
							XSSFCell cell_obj = row_obj.getCell(0);
							if(cell_obj!=null){
								String keynm = cell_obj.toString();
								if(keynm!=null && keynm.equalsIgnoreCase(key)){
									keys = true;
									int  cell_count = row_obj.getLastCellNum();
									for(int j=1; j<=cell_count; j++){
										XSSFCell cellvalue = row_obj.getCell(j);
										String keyvalue = cellvalue.toString();
										if(keyvalue!=null && keyvalue.equalsIgnoreCase(value)){
											values = true;
											exe_rst_status = 1;
											Logger.info("Key "+keynm +" and keyvalue "+keyvalue);
											Logger.info("Key Value matched!!");
											break;
										}
									}
									break;
								}
							}
						}
					}
					
					if(keys!=true){
						Logger.info("key "+key +" is not found !!");
					}
					if(values!=true){
						Logger.info("key "+key +" is found but value "+value+" is not matched");
					}
					
					Workbook_obj.close();
				}else{
					Logger.info("filepath does not exist!!");
				}
			}
			else {
				Logger.info("Contains null path");
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void validateOperation(String OperationName) {
		
		if (OperationName.equalsIgnoreCase("element_check")) {
			validateElementPresence(Driver.OR_ObjectName);
		} 
		else if (OperationName.equalsIgnoreCase("excel_value")) {
			Logger.info("Under excel validation!!");
			String keyname  = fn_Data("keyName");
			String keyvalue = fn_Data("keyValue");
			String filename = fn_Data("filename");
			String sheetname = fn_Data("sheetname");
			// return file path
			String filepath = returnfilePath(filename);
			validatekeyValueInExcel(filepath, sheetname, keyname, keyvalue);
		} 
		else {
			GenericFunctions.Validate(Driver.OR_ObjectName, OperationName);
		}
	}
	
	public static void deleteFile(String filename){
		try{
			String dwnPath = Resourse_path.homepath+File.separator+"Downloads"+File.separator+filename;
			String tempPath = Resourse_path.homepath+File.separator+"AppData"+File.separator+"Local"+
			                  File.separator+"Temp"+File.separator+filename;
			String testdataPath = Resourse_path.currPrjDirpath+File.separator+"test-data"+File.separator+filename;
			String [] path = {dwnPath,tempPath,testdataPath};
			
			for(int i=0; i<path.length; i++){
				String pth = path[i];
				File file = new File(pth);
				if(file.exists()){
					file.delete();
					Logger.info("Deleted file : " +pth);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static String returnfilePath(String filename){
		String rpath = null;
		try{
			String dwnPath = Resourse_path.homepath+File.separator+"Downloads"+File.separator+filename;
			String tempPath = Resourse_path.homepath+File.separator+"AppData"+File.separator+"Local"+
			                  File.separator+"Temp"+File.separator+filename;
			String testdataPath = Resourse_path.currPrjDirpath+File.separator+"test-data"+File.separator+filename;
			
			String [] path = {dwnPath,tempPath,testdataPath};
			
			for(int i=0; i<path.length; i++){
				String pth = path[i];
				File file = new File(pth);
				if(file.exists()){
					rpath = pth;
					Logger.info("return filepath : " +pth);
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
		return rpath;
	}
	
	  /**
     * Downloads a file from a URL
     * @param fileURL HTTP URL of the file to be downloaded
     * @param saveDir path of the directory to save the file
     * @throws IOException
     */
	public static void downloadFile() {
		WebElement Object_name = GenericFunctions.Field_obj(Driver.OR_ObjectName);
		String fileURL = Object_name.getAttribute("href");
		 String saveDir = Resourse_path.comp_downloadfoder ;
		try {
			
			int BUFFER_SIZE = 4096;
			URL url = new URL(fileURL);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			int responseCode = httpConn.getResponseCode();

			// always check HTTP response code first
			if (responseCode == HttpURLConnection.HTTP_OK) {
				String fileName = "";
				String disposition = httpConn.getHeaderField("Content-Disposition");
						
//				String contentType = httpConn.getContentType();
//				int contentLength = httpConn.getContentLength();

				if (disposition != null) {
					// extracts file name from header field
					int index = disposition.indexOf("filename=");
					if (index > 0) {
						fileName = disposition.substring(index + 10,disposition.length() - 1);
					}
				} else {
					// extracts file name from URL
					fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,fileURL.length());
				}
/*
				Logger.info("Content-Type = " + contentType);
				Logger.info("Content-Disposition = " + disposition);
				Logger.info("Content-Length = " + contentLength);
				Logger.info("fileName = " + fileName);
*/
				// opens input stream from the HTTP connection
				InputStream inputStream = httpConn.getInputStream();
				String saveFilePath = saveDir + File.separator + fileName;

				// opens an output stream to save into file
				FileOutputStream outputStream = new FileOutputStream(saveFilePath);
				int bytesRead = -1;
				byte[] buffer = new byte[BUFFER_SIZE];
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}

				outputStream.close();
				inputStream.close();
				Logger.info("File downloaded");
			} else {
				Logger.info("No file to download. Server replied HTTP code: "+ responseCode);
			}
			httpConn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void executevbscript(String vbsfile){
		try{
			String folder = Resourse_path.currPrjDirpath+"\\resources\\vbscript\\examples\\";
			String script = folder+vbsfile+".vbs";
			// search for real path:
			String executable = "wscript.exe";
			String cmdArr[] = { executable, script };
			Runtime.getRuntime().exec(cmdArr);
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void executevbscriptwithArguments(String vbsfile, String fname){
		exe_rst_status = 2;
		try{
			String script = vbsfile+".vbs";
			// search for real path:
			String executable = "wscript.exe";
			String cmdArr[] = { executable, script,fname};
			Runtime.getRuntime().exec(cmdArr);
			exe_rst_status = 1;
		}catch(Exception e){
			exe_rst_status = 2;
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void executeOtherOperation(String OperationName){
		exe_rst_status = 2;
		resultmssg = null;
		try{
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			// Action mapping
			map.put("vbscript", 1);
			map.put("new_autoit", 2);
			map.put("switchToIframe", 3);
			map.put("back_to_default_content", 4);
			map.put("presskey", 5);
			map.put("validte_filepresence", 6);
			
			int data = 0;
			if (map.containsKey(OperationName)){
				 data = map.get(OperationName);
			}
			
			String msg = "operation perfomred!!";
			String initialized = "vbscrpit part initialized";
			String finalmsg = OperationName + " " + msg ;
			
			String vbfolderpath = Resourse_path.vbfolderpath;
			String filename = vbfolderpath+fn_Data("filename");
			
			switch(data){
			case 0:
				Logger.info(OperationName+" key does not match!!");
				exe_rst_status = 2;
				break;
			case 1:
				String function_nm = fn_Data("function_name");
				Logger.info(initialized);
				File file = new File(filename+".vbs");
				Logger.info(filename+"exist");
				Logger.info("Executing vbscript..");
				if(file.exists()){
					executevbscriptwithArguments(filename,function_nm);
				}else {
					Logger.info(filename+" does not exist!!");
					exe_rst_status = 2;
				}
				staticwait(5);
				Logger.info("vbscript executed!!");
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;
			case 2:
				Logger.info(msg);
				String del_fn = fn_Data("filename");
				//delte file before open if it exist..
				deleteFile(del_fn);
				staticwait(5);
				executeAutoItOperation(OperationName.toLowerCase());
				staticwait(12);
				exe_rst_status = 1;	
				break;
			case 3:
				switchToIframe(Driver.OR_ObjectName);
				break;
			case 4:
				returnToDefaultContent();
				break;	
			case 5:
				pressKey();
				break;	
			case 6:
				pressKey();
				break;		
			default:
				Logger.info(OperationName+" case is not mapped for this operation");
				exe_rst_status = 2;
				break;	
			}
		}catch(Exception e){
			exe_rst_status = 2;
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void validateFilePresence(){
		try{
			exe_rst_status = 2;
			String filename = fn_Data("filename");
			if(filename!=null){
				//check in download folder
				String [] folderlist = {Resourse_path.comp_downloadfoder,Resourse_path.tempfolder,Resourse_path.prj_download_fld};
				boolean rstatus = false;
				
				for(int i=0; i<folderlist.length; i++){
					String folder = folderlist[i];
					File file = new File(folder+filename);
					if(file.exists()){
						Logger.info(filename+" File is present in :: "+folder);
						resultmssg = filename+" File is present";
						rstatus = true;
						exe_rst_status = 1;
					}
				}
				
				//Assigning result
				if(rstatus!=true){
					Logger.warn(filename+" is not present");
					resultmssg = filename+" File is not present";
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	
	public static void checkExcelSheetPresence(XSSFWorkbook Wbook_obj ){
		try{
			int shrs = 0;
		    // for each sheet in the workbook
			//validating sheet name 
            for (int sh = 0; sh < Wbook_obj.getNumberOfSheets(); sh++) {
            	String shnm = Wbook_obj.getSheetName(sh);
 //             Logger.info("Sheet name: " + shnm);
                Logger.info("Sheet name: " + shnm);
                if(shnm.equalsIgnoreCase(Driver.DriverSheetname)){
                	//Logger.info(Driver.DriverSheetname+" sheetname found!!");
                	Logger.info(Driver.DriverSheetname+" sheetname found!!");
                	shrs=1;
                	break;
                }
            }
            
            if(shrs!=1){
            	//Logger.info(Driver.DriverSheetname+" sheetname not found!!");
            	Logger.info(Driver.DriverSheetname+" sheetname not found!!");
            }
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
	public static void driverCheck(){
		if(driver==null){
			Logger.info("Driver is null");
		}
	}
	
	public static void returnToDefaultContent(){
		exe_rst_status = 2;
		if(driver!=null){
			driver.switchTo().defaultContent();
		}
		exe_rst_status = 1;
	}
	
/** @author - brijesh-yadav
    @Function_Name -  switchToIframe()
    @Description - It switch the driver to frame
    @return WebElemet object
    @Created_Date - 26 Feb 2015
    @Modified_Date - 
 */
 	public static void switchToIframe(String OR_ObjectName) {
 		exe_rst_status = 2;
 		driverCheck();
 		HashMap<String, String> xml_property_storage = XMLReading(OR_ObjectName);

 		// Storing property name to arraylist
 		ArrayList<String> prpstorage = new ArrayList<String>();

 		for (Map.Entry<String, String> m : xml_property_storage.entrySet()) {
 			prpstorage.add(m.getKey());
 		}

 		ArrayList<String> localobjstorg = localObjStorage();

 		String obj = null;
 		boolean exitprcess = false;
 		boolean mapped = false;
 		for (int i = 0; i < prpstorage.size(); i++) {
 			obj = prpstorage.get(i);
 			// Logger.info("first "+obj);
 			for (int j = 0; j < localobjstorg.size(); j++) {
 				String obj2 = localobjstorg.get(j);
 				if (obj.equalsIgnoreCase(obj2)) {
 					mapped = true;
 					Logger.info(obj+" object successfully mapped!!");
 					String objvalue = xml_property_storage.get(obj.toLowerCase());
 					char[] ch = objvalue.toCharArray();
 					if (ch.length > 0) {
 						Logger.info(obj+" object contains property value !!");
 						exitprcess = true;
 						break;
 					}else{
 						Logger.info(obj+" object contains null property value, so moving to next object..");
 					}
 				}
 			}
 			if (exitprcess != false) {
 				break;
 			}
 		}

 		// object code validation
 		if (exitprcess != false) {

 			String prp_value = xml_property_storage.get(obj);
 			Logger.info("Property name - " + obj);
 			Logger.info("Property value - " + prp_value);

 			HashMap<String, Integer> swtich_prp_storage = switchPrpStorage();

 			// storing property name to hashmap
 			ArrayList<String> switchmapobject = new ArrayList<String>();
 			for (Map.Entry<String, Integer> m : swtich_prp_storage.entrySet()) {
 				switchmapobject.add(m.getKey());
 			}
 			// validating mapping with switch case
 			boolean switchprocess = false;
 			for (int i = 0; i < switchmapobject.size(); i++) {
 				String mpobject = switchmapobject.get(i);
 				if (obj.equalsIgnoreCase(mpobject)) {
 					switchprocess = true;
 					break;
 				}
 			}

 			// swtich case validation
 			if (switchprocess != false) {
 				int switchid = swtich_prp_storage.get(obj);
 				Logger.info(obj + " element is returned!!");
 				Logger.info("Finding "+obj+" element on webpage..");
 				// Assigning element to webelement
 				switch (switchid) {
 				case 1:
 					driver.switchTo().frame(driver.findElement(By.id(prp_value)));
 					exe_rst_status = 1;
 					break;
 				case 2:
 					driver.switchTo().frame(driver.findElement(By.xpath(prp_value)));
 					exe_rst_status = 1;
 					break;
 				case 3:
 					driver.switchTo().frame(driver.findElement(By.className(prp_value)));
 					exe_rst_status = 1;
 					break;
 				case 4:
 					driver.switchTo().frame(driver.findElement(By.name(prp_value)));
 					exe_rst_status = 1;
 					break;
 				case 5:
 					driver.switchTo().frame(driver.findElement(By.linkText(prp_value)));
 					exe_rst_status = 1;
 					break;
 				case 6:
 					driver.switchTo().frame(driver.findElement(By.tagName(prp_value)));
 					exe_rst_status = 1;
 					break;	
 				case 7:
 					driver.switchTo().frame(driver.findElement(By.cssSelector(prp_value)));
 					exe_rst_status = 1;
 					break;
 				case 8:
 					driver.switchTo().frame(driver.findElement(By.partialLinkText(prp_value)));
 					exe_rst_status = 1;
 					break;	
 				default:
 					Logger.info("Event is not mapped for this operation");
 					break;
 				}
 				Logger.info("Element found!!");
 			} else {
 				Logger.info(obj + " is not mapped in property local storage case!!");
 			}

 		} else {
 			if(xml_property_storage.size()>0){
 				if (mapped != true) {
 					Logger.info("Xml object is not mapped in local object storage!!");
 				}else {
 					if(exitprcess!=true){
 						Logger.info("Xml object is mapped but it does not contains object property value in xml file!!");
 					}
 				}
 			}else{
 				Logger.info("size of the element return by xmlreading is "+prpstorage.size());
 			}
 		}

 	}
 	
/** @author - brijesh-yadav
    @Function_Name -  switchToIframe()
    @Description - It switch the driver to frame
    @return WebElemet object
    @Created_Date - 26 Feb 2015
    @Modified_Date - 
 */
 	public static void switchToIframe() {
 		exe_rst_status = 2;
 		String indexvl = fn_Data("frameindex");
 		if(indexvl!=null){
 			char [] in_len = indexvl.toCharArray();
 			if(in_len.length>0){
 				int frameindex = Integer.parseInt(indexvl);
 	 	 		driver.switchTo().frame(frameindex);
 	 	 		exe_rst_status = 1;
 			}
 		}
 	}
 	
/** @author - brijesh-yadav
    @Function_Name -  
    @Description - 
    @return 
    @Created_Date - 
    @Modified_Date - 
 */
	public static void pressKey(){
		try{
			exe_rst_status = 2;	
			String key_nm = fn_Data("key");
			
			if(key_nm!=null){
				
				HashMap<String, Integer> map = new HashMap<String, Integer>();
				// Action mapping
				map.put("tab", 1);
				map.put("enter", 2);
				map.put("down", 3);
				
				// switching data operation
				int data = map.get(key_nm.toLowerCase());
				String msg = "operation perfomred!!";
				
				String stringloop = fn_Data("times");
				int loop = 1; 
				if(stringloop!=null){
					loop = Integer.parseInt(stringloop);
				}
				
				Robot robot = new Robot();
				switch(data){
				case 1:
					for(int i=1; i<=loop; i++){
						robot.keyPress(KeyEvent.VK_TAB);
						robot.keyRelease(KeyEvent.VK_TAB);
						Thread.sleep(1000);
					}
					Logger.info(key_nm + " " + msg);
					exe_rst_status = 1;	
					break;
				case 2:
					Logger.info(key_nm + " " + msg);
					exe_rst_status = 1;	
					break;
				case 3:
					break;
				case 4:
					Logger.info(key_nm + " " + msg);
					exe_rst_status = 1;	
					break;
				default:
					Logger.info(key_nm+" case is not mapped for this operation");
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			Logger.error(e);
		}
	}
	
    public static void fillcommentinexcel(Workbook wbook_obj,Sheet wsheet_obj,Row row_obj,Cell cell, String exmsg){
		try {

			CreationHelper factory = wbook_obj.getCreationHelper();
			Drawing drawing = ((org.apache.poi.ss.usermodel.Sheet) wsheet_obj).createDrawingPatriarch();
			System.out.println("exmsg "+exmsg);
			String errormsg = exmsg;
			char[] ch = errormsg.toCharArray();
			int col = 3;
			int rowDimension = 5;
			System.out.println("ch "+ch.length);
			//providing area to comment box based on character length
			if (ch.length > 100 && ch.length <= 500) {
				col += 3;
				rowDimension += 3;
			} else if (ch.length > 500 && ch.length <= 1000) {
				col += 5;
				rowDimension += 6;
			} else if (ch.length > 1000 && ch.length <= 2000) {
				col += 10;
				rowDimension += 18;
			} else if (ch.length > 2000) {
				col += 20;
				rowDimension += 30;
				System.out.println("third");
			}
			// When the comment box is visible, have it show in a 1x3 space
			ClientAnchor anchor = factory.createClientAnchor();
			anchor.setCol1(cell.getColumnIndex());
			anchor.setCol2(cell.getColumnIndex() + col);
			anchor.setRow1(row_obj.getRowNum());
			anchor.setRow2(row_obj.getRowNum() + rowDimension);
			// Create the comment and set the text+author
			Comment comment = drawing.createCellComment(anchor);
			RichTextString str = factory.createRichTextString(errormsg);
			comment.setString(str);
			// Assign the comment to the cell
			cell.setCellComment(comment);
			
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e);
		}
    }
    
    //store stacktrace to string
    public static String getStackTrace(Exception e) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        return result.toString();
      }
    
    public static void createDownloadFolderpath(){
    	try{
    		String path = Resourse_path.date_time_down_folder+Resourse_path.running_path;
    		Logger.info(path);
    		File file = new File(path);
    		if(!file.exists()){
    			file.mkdirs();
    			Logger.info("Download fodler path created!!");
    		}
    		//Assigning download folder path
    		Resourse_path.comp_downloadfoder = path;
    	}catch(Exception e){
    		e.printStackTrace();
			Logger.error(e);
    	}
    }
    
    public static String getDate(){
    	String datevalue = null;
    	DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		// get current date time with Date()
		Date date = new Date();
		datevalue = dateFormat.format(date);
		return datevalue;
    }
    
    public static String returntime(){
    	String time = null;
		DateFormat datetime = new SimpleDateFormat("HH_mm_ss");
		// get current date time with Calendar()
		Calendar cal = Calendar.getInstance();
		time = datetime.format(cal.getTime());
		return time;
    }
    
    public static void createDateTimeFolder(String foldernm){
    	try{
    		String date = getDate();
    		String time = returntime();
    		String down_date_time_folder = Resourse_path.currPrjDirpath+File.separator+foldernm+File.separator
    				      +date+File.separator+"time_"+time+File.separator;
    		
    		File file = new File(down_date_time_folder);
    		if(!file.exists()){
    			file.mkdirs();
    			Logger.info("Download fodler path created!!");
    		}
    		//assigning path to static string
    		Resourse_path.date_time_down_folder=down_date_time_folder;
    		Logger.info("date_time_down_folder "+Resourse_path.date_time_down_folder);
    	}catch(Exception e){
    		e.printStackTrace();
			Logger.error(e);
    	}
    }
    
    //store values to static till complete execution
    public static void session_setvalue_single(){
    	String flag_v = fn_Data("flag_a");
    	if(flag_v!=null){
    		Resourse_path.flag_a=flag_v;
    	}
    }
    
    public static void vm_mouseclick(String OperationName){
    	try{
    		HashMap<String, Integer> map = new HashMap<String, Integer>();
			// Action mapping
			map.put("vbscript", 1);
			map.put("new_autoit", 2);
			
			int data = 0;
			if (map.containsKey(OperationName)){
				 data = map.get(OperationName);
			}
			
			String msg = "operation perfomred!!";
			String finalmsg = OperationName + " " + msg ;
			String init = OperationName + " initialized!!" ;
			
			switch(data){
			case 0:
				Logger.info(OperationName+" key does not match!!");
				exe_rst_status = 2;
				break;
			case 1:
				Logger.info(init);
				//code goes here
				
				Logger.info(finalmsg);
				exe_rst_status = 1;
				break;
			case 2:
				Logger.info(init);
				//code goes here
				
				Logger.info(finalmsg);
				exe_rst_status = 1;	
				break;
			default:
				Logger.info(OperationName+" case is not mapped for this operation");
				exe_rst_status = 2;
				break;	
			}
    	}catch(Exception e){
    		e.printStackTrace();
    		Logger.error(e);
    	}
    }
    
    
    
}
