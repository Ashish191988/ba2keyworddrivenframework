package driver;

import genericclasses.GenericFunctions;
import genericclasses.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;
import configuration.Resourse_path;

public class Driver {

	//static variables 
	public static String StepNumber = "";
	public static String DriverSheetname="";
	public static String Testcasename = "";
	public static String TestData_Sheetpath=null;
	public static String KeywordName = "";
	public static String SuiteName = "";
	public static String ORXMLName = "";
	public static Integer executing_row = null;
	public static String OR_ObjectName = "";
	public static String ScreenName = "";
	public static String TestStatus = "Passed";
	public static String S_No = "";
	public static String start_time = "";
	public static String End_Time = "";
	public static String HL_RSheetName = "";
	public static String HighLevel_Result_Sheetpath = "";
	public static String LowLeveL_Result_Sheetpath = "";
	public static String LowLevel_Result_Folder = "";
	public static String ORNAME_XML="";
	public static String Snap_flag = "";
	public static String Snap_URL="dummy";
	public static String Parentwindow = null;
	public static String browsername = null;

	// To store results e.g. Passed or Failed
	public static ArrayList<String> Arr_list; 
	public static ArrayList<String> FL = null;

	public static void main(String[] args) {
		try {
			executeTestCases();
		} catch (Exception e) {
            e.printStackTrace();
		}
	}
	
	@Test
	public static void executeTestCases() throws IOException, InvalidFormatException {
		//creating log file 
		Logger.createLogFile();
		//version
		Logger.info("Selenium version :: "+ Resourse_path.selenium_version);
		//force quit the IEDriver process thread
		GenericFunctions.killProcess("IEDriverServer");
		//Create date folder
		GenericFunctions.createDateTimeFolder("Downloads");
		
		Arr_list = new ArrayList<String>();
		Arr_list.add("Passed"); // Initiating Arr_list with Passed value
		// Arraylist contains list of application which has to be executed, data coming from suite Driver sheet
		ArrayList<String> All = GenericFunctions.ReadDriverSuiteExcel(); 
		int len = All.size();
		
		Logger.info("Total number of applications :: "+ len);

		for (int i = 0; i < len; i++) {
			int counter = 0;
			DriverSheetname = (String) All.get(i);
			HL_RSheetName = DriverSheetname;
			Snap_flag = GenericFunctions.Snp_flag.get(i).toString();
			
			Logger.info("Application Name - "+DriverSheetname);
			TestData_Sheetpath = Resourse_path.TestData_Sheetpath+DriverSheetname+".xlsx";
			Logger.info("TestData_Sheet path:: "+TestData_Sheetpath);
			FileInputStream FIS = new FileInputStream(TestData_Sheetpath);
            
			//Excel operation
			XSSFWorkbook Wbook_obj = new XSSFWorkbook(FIS);
			GenericFunctions.checkExcelSheetPresence(Wbook_obj);
            //closing file excel object
			XSSFSheet Wsheet_obj = Wbook_obj.getSheet(DriverSheetname);

			String PrevTestID = null;
			String CurrentTestID="";
			String nextTestID="";
			XSSFRow rowobj = null;
			XSSFRow nextRowObj = null;
			XSSFCell Driver_obj_1, Driver_obj_2, Driver_obj_3;
			@SuppressWarnings("unused")
			String Condition;
					
            //Row count operation
			int Row_c = Wsheet_obj.getLastRowNum()+1;
			Logger.info("Total row " + Row_c);
			int introw = 0;
			for (int f = 1; f < Row_c; f++) {
				boolean rw_rs = GenericFunctions.isRowEmptyInExcel(Wsheet_obj.getRow(f));
				if (rw_rs != true) {
					introw++;
				}
			}
			
            //final row count
			int Row_count = introw;
			Logger.info("After row validation for blank or null!!");
			Logger.info("Total final row " + Row_count);
			//TestCase execute operation
			for (int k = 1; k <= Row_count; k++) {
				
				try {
					
					if (k <= Row_count) {
						rowobj = Wsheet_obj.getRow(k);
						Driver_obj_1 = rowobj.getCell(0);
						Condition = Driver_obj_1.getStringCellValue();
						Driver_obj_2 = rowobj.getCell(1);
						CurrentTestID = Driver_obj_2.getStringCellValue();
						
						//checking row 
						if ((k + 1) <= Row_count) {
							nextRowObj = Wsheet_obj.getRow(k + 1);
							Driver_obj_3 = nextRowObj.getCell(1);
							nextTestID = Driver_obj_3.getStringCellValue();
						} else
							nextTestID = null;
					}
					
					if (PrevTestID == null && k <= Row_count) {
						//System.out.println("PrevTestID - "+PrevTestID +" == null && row "+k +"<= "+Row_count);
						Logger.info("PrevTestID - "+PrevTestID +" == null && row "+k +"<= "+Row_count);
						Date dte = new Date();
						DateFormat df = DateFormat.getTimeInstance();
						String S_time = df.format(dte);
						start_time = S_time;
						counter = counter + 1;
						S_No = Integer.toString(counter);
						PrevTestID = CurrentTestID;
					}
					
					//display previous test step id
					int teststep = k-1;
					Logger.info("Test ID "+CurrentTestID+" : previous test step/row "+teststep+" status is "+TestStatus+" and value of next test step/row is :: "+k);
					Resourse_path.running_path = DriverSheetname+File.separator+CurrentTestID+File.separator;
					
//					GenericFunctions.createDownloadFolderpath();
					
					if ((!(k>Row_count)) && TestStatus.equalsIgnoreCase("passed")){
						
						executing_row = k;
//						System.out.println("\n");
						Logger.info("\n");
//						System.out.println("-------------------- Executing Testcase ID "+CurrentTestID+" and test step : "+k+" ---------------------- ");
						Logger.info("-------------------- Executing Testcase ID "+CurrentTestID+" and test step : "+k+" ---------------------- ");
						
						XSSFCell Cell_obj_0 = rowobj.getCell(0);
						String StepNum = Cell_obj_0.getStringCellValue();

						XSSFCell Cell_obj = rowobj.getCell(1);
						String Excel_Tcid = Cell_obj.getStringCellValue();

						XSSFCell Cell_obj2 = rowobj.getCell(3);
						String ORName = Cell_obj2.getStringCellValue();

						XSSFCell Cell_obj4 = rowobj.getCell(3);
						String Screen_Name = Cell_obj4.getStringCellValue();

						XSSFCell Cell_obj1 = rowobj.getCell(4);
						String Keyword = Cell_obj1.getStringCellValue();

						String ObjectNm="";
						
						XSSFCell Cell_obj9 = rowobj.getCell(5);
						if (Cell_obj9!=null){
							ObjectNm = Cell_obj9.getStringCellValue();	
						}

						XSSFCell Cell_ORNAME = rowobj.getCell(2);
						ORNAME_XML = Cell_ORNAME.getStringCellValue();
						StepNumber = StepNum;
						// System.out.println(StepNumber);
						Testcasename = Excel_Tcid;
						// System.out.println(Testcasename);
						KeywordName = Keyword;
						// System.out.println(Keyword);
						SuiteName = DriverSheetname;
						ORXMLName = ORName;
						ScreenName = Screen_Name.trim();
						OR_ObjectName = ObjectNm;
						// System.out.println(ObjectNme);

						String SheetNameCheck="";
						String ResultSheetname="";
						
						if (!SheetNameCheck.equalsIgnoreCase(HL_RSheetName)){
							ResultSheetname=HL_RSheetName+"_"+Resourse_path.DateTimeStamp;
							SheetNameCheck=HL_RSheetName;
						}

						HighLevel_Result_Sheetpath = Resourse_path.currPrjDirpath
						+ "/Results/"
						+ "/"
						+ "Result_"
						+ ResultSheetname + ".xlsx"; 
				
						LowLeveL_Result_Sheetpath = Resourse_path.currPrjDirpath
						+ "/Results/"
						+ "/"
						+ "Result_"
						+ ResultSheetname+ ".xlsx";

						LowLevel_Result_Folder = Resourse_path.currPrjDirpath
						+ "/Results/" ;
						
						//Adding test data to array
						ArrayList<String> Bl = GenericFunctions.FindTestData();
						FL = Bl;
						//
						//Handle multiple windows
						int wincnt = GenericFunctions.winCount();
						String act_wind = GenericFunctions.fn_Data("activateWindow");
						
						if (wincnt>1){
//							System.out.println("More than 1 window exist, so switching to another window!!");
							Logger.info("More than 1 window exist, so switching to another window!!");
							if(Keyword.equalsIgnoreCase("close")){
//								GenericFunctions.handleMultipleWindowsforClosingwindow();
							}
							else if(Keyword.equalsIgnoreCase("other")){
//								System.out.println("not doing anything!!");
								Logger.info("not doing anything!!");
							}
							else if(Keyword.equalsIgnoreCase("wait")){
//								System.out.println("not doing anything!!");
								Logger.info("not doing anything!!");
							}
							else if(Keyword.equalsIgnoreCase("openurl")){
//								System.out.println("not doing anything!!");
								Logger.info("not doing anything!!");
							}
							else {
								GenericFunctions.windowInstancehandler("pageObject");
							}
						}else if(wincnt==1) {
							if(act_wind!=null){
								if(act_wind.equalsIgnoreCase("1")){
									//get instance of parent window
									if(Keyword.equalsIgnoreCase("close")){
//										GenericFunctions.handleMultipleWindowsforClosingwindow();
									}
									else if(Keyword.equalsIgnoreCase("other")){
//										System.out.println("not doing anything!!");
										Logger.info("not doing anything!!");
									}
									else if(Keyword.equalsIgnoreCase("wait")){
//										System.out.println("not doing anything!!");
										Logger.info("not doing anything!!");
									}
									else if(Keyword.equalsIgnoreCase("openurl")){
										Logger.info("not doing anything!!");
//										System.out.println("not doing anything!!");
									}
									else {
										GenericFunctions.windowInstancehandler("pageObject");
									}
								}
							}
						}
						
						if (Keyword.equalsIgnoreCase("Login")) {
							String conditional_cl = GenericFunctions.fn_Data("conditional_click");
							if(conditional_cl!=null){
								boolean sts = GenericFunctions.validateElementPresence(conditional_cl);
								if(sts!=true){
									GenericFunctions.Login(GenericFunctions.fn_Data("UserName"),GenericFunctions.fn_Data("PASSWORD"));
								}
							}else {
								GenericFunctions.Login(GenericFunctions.fn_Data("UserName"),GenericFunctions.fn_Data("PASSWORD"));
							}
							
							int resultst = GenericFunctions.exe_rst_status;
						    reportResult(resultst,"User should be logged-In","Login Successful","User failed while login.");
							    
						} else if (Keyword.equalsIgnoreCase("Validate")) {
							
							XSSFCell Cell_obj5 = rowobj.getCell(5);
							XSSFCell Cell_obj6 = rowobj.getCell(6);

							if (Cell_obj5 != null) {
								String ObjectName = Cell_obj5.getStringCellValue();
								OR_ObjectName = ObjectName;
							}
							
							ArrayList<String> arr = new ArrayList<String>();
							arr = GenericFunctions.returnArraylistStringCommaSeprated(OR_ObjectName);
							if(arr.size()>1 && Cell_obj6!=null){
								GenericFunctions.handleDynamicOperation("Validate",Cell_obj6.getStringCellValue());
							} 
							else if (Cell_obj6 != null && arr.size()<=1) {
								//conditions
									if (OR_ObjectName.equalsIgnoreCase("Operator")) {
										XSSFCell Cell_obj12 = rowobj.getCell(6);
										XSSFCell Cell_obj13 = rowobj.getCell(7);
										XSSFCell Cell_obj14 = rowobj.getCell(8);
										XSSFCell Cell_obj15 = rowobj.getCell(9);
										//storing cell values to variable
										String ObjectName = Cell_obj15.getStringCellValue();
										OR_ObjectName = ObjectName;
										String operator = Cell_obj12.getStringCellValue();
										String VariableType = Cell_obj13.getStringCellValue();
										String Value = Cell_obj14.getStringCellValue();
										GenericFunctions.Validate(OR_ObjectName,operator, VariableType, Value);
									} 
									else if (Cell_obj5 != null && Cell_obj6 != null) {
										String operationname = Cell_obj6.getStringCellValue();
										GenericFunctions.validateOperation(operationname);
									} 
									else if (Cell_obj5 == null && Cell_obj6 != null) {
										String operationname = Cell_obj6.getStringCellValue();
										GenericFunctions.validateOperation(operationname);
									}
							}
							else if (Cell_obj5 == null && Cell_obj6 == null) {
								GenericFunctions.Validate();
							}
							else {
				//				System.out.println("Some mapping problem occured !!");
								Logger.warn("Some mapping problem occured !!");
							}
							
							int resultst = GenericFunctions.exe_rst_status;
							String rsmssg = GenericFunctions.returnresultmssg();
						    reportResult(resultst,"Data should be validated",rsmssg);
							
						} else if (Keyword.equalsIgnoreCase("INPUT")) {
						
							XSSFCell Cell_obj3 = rowobj.getCell(5);
							String ObjectName = Cell_obj3.getStringCellValue();
							XSSFCell Cell_obj5 = rowobj.getCell(6);
							String ValueToPerformOperation="";
							
							if (Cell_obj5!=null) {					
							 ValueToPerformOperation = Cell_obj5.getStringCellValue();
							}
							
							OR_ObjectName = ObjectName;							
							GenericFunctions.INPUT(OR_ObjectName,ValueToPerformOperation);
							int resultst = GenericFunctions.exe_rst_status;
						    reportResult(resultst,"Input operation should be performed","Input operation is performed","Not performed");
						}
						else if (Keyword.equalsIgnoreCase("Click")) {
						
							XSSFCell Cell_obj5 = rowobj.getCell(5);
							XSSFCell Cell_obj6 = rowobj.getCell(6);
							
						    if (Cell_obj5!=null){
								String ObjectName = Cell_obj5.getStringCellValue();
								OR_ObjectName = ObjectName;	
								
								ArrayList<String> arr = new ArrayList<String>();
								arr = GenericFunctions.returnArraylistStringCommaSeprated(OR_ObjectName);
//								System.out.println("page object present in excel : "+arr.size());
								Logger.info("page object present in excel : "+arr.size());
								
								if(arr.size()>=2 && Cell_obj6!=null){
//									System.out.println("Under multiple object handle operation!!");
									Logger.info("Under multiple object handle operation!!");
									GenericFunctions.handleDynamicOperation("click",Cell_obj6.toString());
								}
								else if(arr.size()==1){
									GenericFunctions.clickCondition(OR_ObjectName, Cell_obj6);
								}
						    }else {
						    	//other operation
						    	GenericFunctions.OtherClickOperation(Cell_obj6);
						    }
						    int resultst = GenericFunctions.exe_rst_status;
						    reportResult(resultst,OR_ObjectName+" Object should be clicked",OR_ObjectName+" Object is clicked",OR_ObjectName+" Object is not clicked");
							
						} else if (Keyword.equalsIgnoreCase("wait")) {
							GenericFunctions.waitOperation(rowobj.getCell(5), rowobj.getCell(6));
						    int resultst = GenericFunctions.exe_rst_status;
						    String resutmsg = GenericFunctions.resultmssg;
						    reportResult(resultst,"Application should wait!!",resutmsg);
						}
						else if (Keyword.equalsIgnoreCase("close")) {
							GenericFunctions.Close();
							int resultst = GenericFunctions.exe_rst_status;
						    reportResult(resultst,"Window Should be Closed","Window closed successfully","Window not closed");
						}
						else if (Keyword.equalsIgnoreCase("other")) {
							String action = GenericFunctions.fn_Data("action");
							if(action!=null){
								String validtedata = action;
								char[] data = validtedata.toCharArray();
								if(data.length>0){
									GenericFunctions.executeOtherOperation(validtedata);
								}
							}
							int resultst = GenericFunctions.exe_rst_status;
							String resutmsg = GenericFunctions.resultmssg;
							reportResult(resultst,action+" Operation should be success !!",resutmsg);
						}
											
						else if (Keyword.equalsIgnoreCase("OpenURL")) {
							String Browser;				
							XSSFCell Cell_obj6 = rowobj.getCell(6);
							String URL = Cell_obj6.getStringCellValue();
							try{
								XSSFCell Cell_obj8 = rowobj.getCell(8);
								Browser = Cell_obj8.getStringCellValue();
								browsername = Browser;
							}catch(Exception e){
								Browser="none";
								browsername = Browser;
							}
							
							URL = URL.trim();
							GenericFunctions.OpenURL(URL,Browser);
							int resultst = GenericFunctions.exe_rst_status;
						    reportResult(resultst, "Url should be launached", "Url is launched successfully","Url is not launched");
						}
						
						else if(Keyword.equalsIgnoreCase("FileUpload")){
							XSSFCell Cell_obj3 = rowobj.getCell(5);
							String ObjectName = Cell_obj3.getStringCellValue();
							XSSFCell Cell_obj5 = rowobj.getCell(6);
							String Path="";
							
							if (Cell_obj5!=null) {					
								Path = Cell_obj5.getStringCellValue();
							}
							
							OR_ObjectName = ObjectName;
							GenericFunctions.fileUpload(OR_ObjectName, Path);
							//GenericFunctions.INPUT(OR_ObjectName,ValueToPerformOperation);
							int resultst = GenericFunctions.exe_rst_status;
						}
						//clearing the find test data for current row
						Bl.clear();
					}
					
				}
				catch (Exception e) {
					Logger.warn("exceptionHandler called!!");
					String messagedata = GenericFunctions.getStackTrace(e);
					exceptionreport("Object should be found","Object not found!!", messagedata);
					Logger.error(e);
					e.printStackTrace();
				}
				finally {
					
					if (CurrentTestID.equalsIgnoreCase(nextTestID)){
						if (TestStatus.equalsIgnoreCase("failed"))
							continue;
					} else if(((CurrentTestID!=nextTestID) || (nextTestID==null)) && (k<=Row_count)){
//						System.out.println("Row "+CurrentTestID+"!="+nextTestID+" || ("+nextTestID+"==null)) && ("+k+"<="+Row_count+")");
						Logger.info("Row "+CurrentTestID+"!="+nextTestID+" || ("+nextTestID+"==null)) && ("+k+"<="+Row_count+")");
						if(TestStatus.equalsIgnoreCase("Passed") || TestStatus.equalsIgnoreCase("Failed")){
							Logger.info("Status :: "+TestStatus+" :: Finalizing result report");
							PrevTestID = null;
							// System.out.println("End time for previous");
							Date dte = new Date();
							DateFormat df = DateFormat.getTimeInstance();
							String E_Time = df.format(dte);
							End_Time = E_Time;
							try {
								int Length_status = Arr_list.size();
								for (int m = 0; m < Length_status; m++) {
									String Status = (String) Arr_list.get(m);
									//System.out.println("Status "+Status);
									if (Status.equalsIgnoreCase("Failed")) {
										TestStatus = "Failed";
										break;
									} else {
										TestStatus = "Passed";
									}
								}
								String[] Array = { Driver.S_No,Driver.Testcasename, Driver.start_time,Driver.End_Time, Driver.TestStatus };
								GenericFunctions.Write_HL_ResultToExcel(Array,Driver.HL_RSheetName);
								Arr_list.clear();
								GenericFunctions.DrawGraph(Driver.HL_RSheetName);
								
								if (CurrentTestID != nextTestID){
									TestStatus = "passed";
								}
							} catch (Exception e) {
								e.printStackTrace();
//								System.out.println("Exception in driver class:: "+ e.getMessage());
                                Logger.error(e);
							}
							
							if(TestStatus.equalsIgnoreCase("Failed")){
								PrevTestID=null;
								TestStatus="passed";
							}
						}
						
					}
					
					if (k==Row_count){
						if(GenericFunctions.driver!=null){
							GenericFunctions.driver.quit();
							GenericFunctions.driver=null;
							Logger.info("Driver is quit");
							Logger.info("Execution for "+DriverSheetname+" is completed..Now executing next application..");
							Logger.info("\n");
							Logger.info("/*************************************************************************/");
							Logger.info("/**************   Execution completed           **************************/");
							Logger.info("/**************************************************************************/");
							Logger.info("\n");
						}
						//Assigning null values to driver on complete execution
						GenericFunctions.driver=null;
					}
					Wbook_obj.close();
				}
			}
			//Need to add code here for send email
		}
		
		//force quit the IEDriver process thread
		GenericFunctions.killProcess("IEDriverServer");
		//Force quit to java execution
		System.exit(-1);
	}
	
	//add step report to excel
	public static void reportResult(int resultst,String Expectedmsg, String resultmssg){
		
		try{
			if (GenericFunctions.winCount()>=1){
				if(GenericFunctions.driver!=null){
					if (Snap_flag.equalsIgnoreCase("always")) {
						Snap_URL=GenericFunctions.Fn_TakeSnapShotAndRetPath(GenericFunctions.driver);
					}
				}
			}
			Logger.info("restult status : "+resultst);
		    if(resultst==1){
				String[] Resultarray = { Driver.Testcasename,
						Driver.StepNumber, Driver.HL_RSheetName,
						Driver.KeywordName, Driver.ScreenName,Expectedmsg,resultmssg,"Passed",Snap_URL};
				GenericFunctions.WriteResultToExcel(Resultarray,Driver.HL_RSheetName);
				Arr_list.add("Passed");
				TestStatus = "Passed";
		    }else if(resultst==2){
				String[] Resultarray = { Driver.Testcasename,
						Driver.StepNumber, Driver.HL_RSheetName,
						Driver.KeywordName, Driver.ScreenName,
						Expectedmsg,resultmssg,"Failed",Snap_URL};
				GenericFunctions.WriteResultToExcel(Resultarray,Driver.HL_RSheetName);
				Arr_list.add("Failed");
				TestStatus = "Failed";
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//add step report to excel
	public static void reportResult(int resultst, String Expectedmsg, String resultpssmssg, String resultfailmssg){
		try{
			if (GenericFunctions.winCount()>=1){
				if(GenericFunctions.driver!=null){
					if (Snap_flag.equalsIgnoreCase("always")) {
						Snap_URL=GenericFunctions.Fn_TakeSnapShotAndRetPath(GenericFunctions.driver);
					}
				}
			}
			Logger.info("restult status : "+resultst);
		    if(resultst==1){
				String[] Resultarray = { Driver.Testcasename,
						Driver.StepNumber, Driver.HL_RSheetName,
						Driver.KeywordName, Driver.ScreenName,Expectedmsg,resultpssmssg,"Passed", Snap_URL};
				
				GenericFunctions.WriteResultToExcel(Resultarray,Driver.HL_RSheetName);
				Arr_list.add("Passed");
				TestStatus = "Passed";
				
		    }else if(resultst==2){
				String[] Resultarray = { Driver.Testcasename,
						Driver.StepNumber, Driver.HL_RSheetName,
						Driver.KeywordName, Driver.ScreenName,
						Expectedmsg,resultfailmssg,"Failed",Snap_URL};
				GenericFunctions.WriteResultToExcel(Resultarray,Driver.HL_RSheetName);
				Arr_list.add("Failed");
				TestStatus = "Failed";
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//add step report to excel
	public static void exceptionreport(String Expectedmsg, String resultmssg, String excmsg){
		try{
			if (GenericFunctions.winCount()>=1){
				if(GenericFunctions.driver!=null){
					if (Snap_flag.equalsIgnoreCase("always")) {
						Snap_URL=GenericFunctions.Fn_TakeSnapShotAndRetPath(GenericFunctions.driver);
					}
				}
			}
		   String[] Resultarray = { Driver.Testcasename,
					Driver.StepNumber, Driver.HL_RSheetName,
					Driver.KeywordName, Driver.ScreenName,
					Expectedmsg,resultmssg,"Failed",Snap_URL,excmsg};
			GenericFunctions.WriteResultToExcel(Resultarray,Driver.HL_RSheetName);
			Arr_list.add("Failed");
			TestStatus = "Failed";
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
