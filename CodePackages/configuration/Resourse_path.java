package configuration;

import java.io.File;
import genericclasses.GenericFunctions;
import driver.Driver;

public class Resourse_path {
	
	public static String DateStamp = GenericFunctions.fn_GetCurrentDate();
	
	public static String DateTimeStamp = GenericFunctions.fn_GetCurrentTimeStamp();
	
	public static String applicationresultfolder = Driver.HL_RSheetName;
	
	public static String ie_32bit = "32bit_2.45.0";
	
	public static String ie_64bit = "64bit_2.42.0";
		
	public static String testcaseresultfolder = Driver.Testcasename;
		
	public static String currPrjDirpath = System.getProperty("user.dir"); //To get current directory path
	
	public static String homepath = System.getProperty("user.home"); //To get home path
	
	public static String Driver_Sheetpath = currPrjDirpath+"/Schedular/Suite Driver.xlsx"; //To get Driver Sheet path
	
	public static String Driver_SheetName = "Driver"; // To get Driver Sheet tab name
	
	public static String TestData_Sheetpath = currPrjDirpath+"/TestCases/"; //To get test data sheet path
	
	public static String browserDriverpath = currPrjDirpath+"/resources/browser-drivers/"+ie_64bit+File.separator;
	
	public static String autoItpath = currPrjDirpath+"/resources/autoIt/compiled/";
	
	public static String vbfolderpath = currPrjDirpath+"/resources/vbscript/";
	
	public static String browsertoolpath = currPrjDirpath+"/resources/tool/";
	
	public static String Result_Sheet_header[]= {"TestcaseName","TestStepNumber" , "ApplicationName", "TestStep" , "Screen", "ExpectedResult" , "ActualResult" , "Status" , "SnapshotLink"}; // Detailed result sheet columns

	public static String HighLevel_Result_header[] = {"SR.No" , "TestCaseName", "Start Time" , "End Time", "Status"}; // High level result sheet column
	
	public static String downloadfolder = currPrjDirpath+"/downloads/";
	
	public static String logfilepath = "";
	
	public static String downloadfoder = Resourse_path.homepath+File.separator+"Downloads"+File.separator;
	
	public static String tempfolder = Resourse_path.homepath+File.separator+"AppData"+File.separator+"Local"+
                                       File.separator+"Temp"+File.separator;
	
	public static String prj_download_fld = Resourse_path.currPrjDirpath+File.separator+"Downloads"+File.separator;
	
}
